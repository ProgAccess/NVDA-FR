# NVDA-FR

This is the source code of [the NVDA-FR website](https://www.nvda-fr.org).

This code source is mainly based on [ProgAccess code source](https://gitlab.com/ProgAccess/ProgAccess), but, as this website is only for french community, some features like translation has been removed.
Some other small adjustments are made.

This repository exists for simplifying both development, contribution and source code publication.

This repository does not contain the editorial content of NVDA-FR. Articles and files are distributed via the website. Articles are under CC BY-SA 4.0.

## Support

## Contribution

MR and issues welcome!

If you want to help (report bugs...) but don't know how to use Git or GitLab, just send a message via [the contact form](https://www.nvda-fr.org/contacter.php).

In case you've found a security issue, please also use the contact form instead of GitLab.

## Installation

NVDA-FR is not designed to run on any other instance than the official ones (dev and prod). Support is not guaranteed for any other instance. **The following instructions are not exhaustive.**

Dependencies:
* PHP
* PHP PDO
* MySQL
* Apache or Nginx
* cron (or any alternative)
* SMTP server

Steps:
* This repository has to be the server root.
* The folders `.`, `files`, `cache` have to be writable by PHP.
* Create MySQL database and tables.
* Copy `inclus/config.php` to `inclus/config.local.php` and edit the copy. Ensure that this file is not readable from the network.
* Create cron jobs for the files in `tasks` (TODO: add crontab to repo)

## License

### Source code

CopyLeft 2006-2021 Team NVDA-FR and ProgAccess

NVDA-FR is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

NVDA-FR is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with NVDA-FR. If not, see https://www.gnu.org/licenses/.

### Libraries

This repository contains some libraries that may have a different license. In that case, the copyright notice and license should be in the library's folder.
