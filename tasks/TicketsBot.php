<?php
$document_root = __DIR__.'/..';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require_once($document_root.'/include/lib/phpmailer/src/PHPMailer.php');
require_once($document_root.'/include/lib/phpmailer/src/Exception.php');
require_once($document_root.'/include/lib/phpmailer/src/SMTP.php');
require_once($document_root.'/include/consts.php');
require_once($document_root.'/include/MDConverter.php');
  $mbox = imap_open (IMAP_INBOX, TICKETS_BOT_MAIL, TICKETS_BOT_PSW);
function getBody($uid, $imap) {
    $body = html_entity_decode(get_part($imap, $uid, "TEXT/HTML"));
    // if HTML body is empty, try getting text body
    if ($body == "") {
        $body = nl2br(get_part($imap, $uid, "TEXT/PLAIN"));
    }
    return explode("## Ne pas écrire en-dessous de cette ligne ##", $body)[0];
}

function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false) {
    if (!$structure) {
           $structure = imap_fetchstructure($imap, $uid, FT_UID);
    }
    if ($structure) {
        if ($mimetype == get_mime_type($structure)) {
            if (!$partNumber) {
                $partNumber = 1;
            }
            $text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
            switch ($structure->encoding) {
                case 3: return imap_base64($text);
                case 4: return imap_qprint($text);
                default: return imap_utf8($text);
           }
       }

        // multipart 
        if ($structure->type == 1) {
            foreach ($structure->parts as $index => $subStruct) {
                $prefix = "";
                if ($partNumber) {
                    $prefix = $partNumber . ".";
                }
                $data = get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
                if ($data) {
                    return $data;
                }
            }
        }
    }
    return false;
}

function get_mime_type($structure) {
    $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

    if ($structure->subtype) {
       return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
    }
    return "TEXT/PLAIN";
}
  $mails=false;
  if ($mbox !== false)
  {
      $info = imap_check($mbox);
  if ($info !== false)
  {
            $mails = imap_fetch_overview($mbox, '1:'.min(50, $info->Nmsgs), 0);
  }
  }
  if ($mails !== false)
  {
  foreach ($mails as $mail)
  {
      $headerText = imap_fetchHeader($mbox, $mail->uid, FT_UID);
      $header = imap_rfc822_parse_headers($headerText);
      $corps = convertToMD(getBody($mail->uid, $mbox));
      if(strpos(iconv_mime_decode($mail->subject), "(Ticket #" !== false))
      {
      $ticketId=explode("(Ticket #", explode("#)", iconv_mime_decode($mail->subject))[0])[1];
      $req = $bdd->prepare('SELECT * FROM `tickets` WHERE `id`=? LIMIT 1');
      $req->execute(array($ticketId));
      $mailAddr=$header->from[0]->mailbox.'@'.$header->from[0]->host;
      if($rdata = $req->fetch())
      {
       if($rdata['expeditor_email'] == $mailAddr)
       {
       $reqU = $bdd->prepare('UPDATE `tickets` SET `messages`=?, `status`=1, `date`=?, `lastadmreply`=?WHERE `id`=? LIMIT 1');
       $messages = json_decode($rdata['messages'], true);
       $messages[] = ['e'=>$rdata['expeditor_name'],'m'=>0,'d'=>time(), 't'=>$corps];
       $reqU->execute(array(json_encode($messages), time(), $rdata['expeditor_name'], $ticketId));
       $body = '<!DOCTYPE html>
       <html lang="fr">
       <head>
       <meta charset="utf-8">
       <title>Re: "'.htmlspecialchars($rdata['subject']).'" '.$site_name.'</title>
       </head>
       <body>
       <p>## Ne pas écrire en-dessous de cette ligne ##</p>
       <h1>'.$site_name.' - Ticket '.$ticketId.'</h1>
       <p>Une réponse a été envoyée via le formulaire de contact de '.$site_name.'.</p>
       <h2>'.$rdata['subject'].' (par '.$rdata['expeditor_name'].')</h2>
       <p>'.$corps.'</p>
       <p><a href="'.SITE_URL.'/admin/tickets.php?ticket='.$ticketId.'">Consultez le ticket</a> ou répondez à ce message sans en modifier l\'objet pour continuer la discussion.</p>
       </body>
       </html>';
       $mailS = new PHPMailer;
       $mailS->isSMTP();
       $mailS->Host = SMTP_HOST;
       $mailS->Port = SMTP_PORT;
       $mailS->SMTPAuth = true;
       $mailS->Username = SMTP_USERNAME;
       $mailS->Password = SMTP_PSW;
       $mailS->setFrom(SMTP_MAIL, $rdata['expeditor_name'].' via '.$site_name);
       $mailS->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
       $reqGAddr = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
       $reqGAddr->execute();
       while($data = $reqGAddr->fetch())
       {
       $reqGAddr2 = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
       $reqGAddr2->execute(array($data['account_id']));
       while($data2 = $reqGAddr2->fetch())
       {
       $mailS->addBCC($data2['email']);
       }
       }
       $mailS->Subject = 'Re: ['.$site_name.'] : '.$rdata['subject'].' (Ticket #'.$ticketId.'#)';
       $mailS->CharSet = 'UTF-8';
       $mailS->isHTML(TRUE);
       $mailS->Body = $body;
       $mailS->send();
       }
       else
       {
       $reqSelectAdm=$bdd2->prepare('SELECT * FROM `accounts` WHERE `email`=? LIMIT 1');
       $reqSelectAdm->execute(array($mailAddr));
       $reqGetAdmName=$bdd2->prepare('SELECT `short_name` FROM `team` WHERE `account_id`=?');
       $reqGetAdmName->execute(array($reqSelectAdm->fetch()['id']));
       $admName=$reqGetAdmName->fetch()['short_name'];
       $reqU = $bdd->prepare('UPDATE `tickets` SET `messages`=?, `status`=2, `date`=?, `lastadmreply`=?WHERE `id`=? LIMIT 1');
       $messages = json_decode($rdata['messages'], true);
       $messages[] = ['e'=>$admName,'m'=>1,'d'=>time(), 't'=>$corps];
       $reqU->execute(array(json_encode($messages), time(), $admName, $ticketId));
       $body = '<!DOCTYPE html>
       <html lang="fr">
       <head>
       <meta charset="utf-8">
       <title>Re: "'.htmlspecialchars($rdata['subject']).'" '.$site_name.'</title>
       <style type="text/css">#response{border-left:1px solid #0080FF;margin-left:8px;padding: 8px;}</style>
       </head>
       <body>
       <p>## Ne pas écrire en-dessous de cette ligne ##</p>
       <h1>'.$site_name.' - Ticket '.$ticketId.'</h1>
       <p>Vous avez reçu une réponse de '.$admName.' pour votre message&nbsp;: <i>'.htmlspecialchars($rdata['subject']).'</i>.</p>
       <div id="response">'.$corps.'</div>
       <hr>
       <p>Pour poursuivre la discussion, utilisez le lien ci-dessous ou répondez simplement à ce message sans en modifier l\'objet.<br>
       <a href="'.SITE_URL.'/contacter.php?reply='.$rdata['id'].'&h='.$rdata['hash'].'">'.SITE_URL.'/contacter.php?reply='.$rdata['id'].'&h='.$rdata['hash'].'</a></p>
       </body>
       </html>';
       $teambody = '<!DOCTYPE html>
       <html lang="fr">
       <head>
       <meta charset="utf-8">
       <title>Re: "'.htmlspecialchars($rdata['subject']).'" '.$site_name.'</title>
       </head>
       <body>
       <p>## Ne pas écrire en-dessous de cette ligne ##</p>
       <h1>'.$site_name.' - Ticket '.$ticketId.'</h1>
       <p>'.$admName.' a répondu au ticket '.$ticketId.' de '.$rdata['expeditor_name'].' ('.htmlspecialchars($rdata['subject']).')</p>
       <p>'.$corps.'</p>
       <a href="'.SITE_URL.'/admin/tickets.php?ticket='.$ticketId.'">Consulter le ticket complet</a> ou répondez à ce message sans en modifier l\'objet pour poursuivre la discussion.</p>
       </body>
       </html>';
       $mailS = new PHPMailer;
       $mailS->isSMTP();
       $mailS->Host = SMTP_HOST;
       $mailS->Port = SMTP_PORT;
       $mailS->SMTPAuth = true;
       $mailS->Username = SMTP_USERNAME;
       $mailS->Password = SMTP_PSW;
       $mailS->setFrom(SMTP_MAIL, $admName.' via '.$site_name);
       $mailS->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
       $mailS->addAddress($rdata['expeditor_email']);
       $mailS->Subject = 'Re: ['.$site_name.'] : '.htmlspecialchars($rdata['subject']).' (Ticket #'.$ticketId.'#)';
       $mailS->CharSet = 'UTF-8';
       $mailS->isHTML(TRUE);
       $mailS->Body = $body;
       $mailS->send();
       $mailS2 = new PHPMailer;
       $mailS2->isSMTP();
       $mailS2->Host = SMTP_HOST;
       $mailS2->Port = SMTP_PORT;
       $mailS2->SMTPAuth = true;
       $mailS2->Username = SMTP_USERNAME;
       $mailS2->Password = SMTP_PSW;
       $mailS2->setFrom(SMTP_MAIL, $admName.' (admin) via '.$site_name);
       $mailS2->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
       $reqGAddr = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
       $reqGAddr->execute();
       while($data = $reqGAddr->fetch())
       {
       $reqGAddr2 = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
       $reqGAddr2->execute(array($data['account_id']));
       while($data2 = $reqGAddr2->fetch())
       {
       $mailS2->addBCC($data2['email']);
       }
       }
       $mailS2->Subject = 'Re: ['.$site_name.'] : '.htmlspecialchars($rdata['subject']).' (Ticket #'.$ticketId.'#)';
       $mailS2->CharSet = 'UTF-8';
       $mailS2->isHTML(TRUE);
       $mailS2->Body = $teambody;
       $mailS2->send();
       }
       imap_delete($mbox, $mail->uid, FT_UID);
      }
      }
  }
  imap_expunge($mbox);
  imap_close($mbox);
  }
?>