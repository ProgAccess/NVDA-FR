<?php
$document_root = __DIR__.'/..';
require_once($document_root.'/include/consts.php');
require_once($document_root.'/include/dbconnect.php');
$cachedir = $document_root.'/cache/';
$time = time();
$ltime = $time - 2678400;# 31 days ago

# Get categories
$cat = array();
$req = $bdd->query('SELECT * FROM `softwares_categories`');
while($data = $req->fetch()) {
	$cat[$data['id']] = $data['name'];
}

# Get softwares
$sft = array();
$req = $bdd->prepare('SELECT * FROM `softwares` WHERE `date`>=?');
$req->execute(array($ltime));
while($data = $req->fetch()) {
	$sft[$data['id']] = $data;
}

# Get files
$files = array();
$req = $bdd->prepare('SELECT * FROM `softwares_files` WHERE `date`>=? ORDER BY `date` DESC');
$req->execute(array($ltime));
while($data = $req->fetch()) {
	$files[date('Y-m-d',$data['date'])][] = $data;
}

# Get days
$days = array();
$curtime = $time;
while($curtime >= $ltime) {
	$days[] = array(date('Y-m-d', $curtime), strftime('%A %e %B', $curtime));
	$curtime -= 86400;
}

# Open files
$file_rss = fopen($document_root.'/journal_modif.xml', 'w');
fwrite($file_rss, '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"><channel><title>'.$site_name.', les dernières nouvelles</title><link>'.SITE_URL.'</link><description>Journal des modifications sur '.$site_name.'.</description><copyright>2006-'.date('Y').' '.$site_name.'</copyright><language>fr</language>');

foreach($days as &$day) {
	$rss = '';
	$title = false;
	$space = false;
	
	
	# Check & write softwares
	if(isset($files[$day[0]])) {
		$title = true;
		$cursfts = array();
		foreach($files[$day[0]] as &$curfile) {
			$cursfts[$curfile['sw_id']][] = $curfile;
		}
		unset($curfile);
		foreach($cursfts as &$cursft) {
			$c = $sft[$cursft[0]['sw_id']];
			foreach($cursft as &$curfile) {
				if($curfile['label'] != '') {
					$rss .= '<item><title>'.$curfile['title'].' ('.$c['name'].', '.$cat[$c['category']].')</title><link>'.SITE_URL.'/dl/'.$curfile['label'].'</link><description>'.$c['description'].'</description><pubDate>'.date('r', $curfile['date']).'</pubDate></item>';
				}
				else {
					$rss .= '<item><title>'.$curfile['title'].' ('.$c['name'].', '.$cat[$c['category']].')</title><link>'.SITE_URL.'/dl/'.$curfile['id'].'</link><description>'.$c['description'].'</description><pubDate>'.date('r', $curfile['date']).'</pubDate></item>';
				}
			}
			unset($curfile);
			$space = true;
		}
		unset($cursft);
	}
	
	# Write
	if($title) {
		fwrite($file_rss, str_replace('{{site}}',$site_name,$rss));
	}
}
$req->closeCursor();
fwrite($file_rss, '</channel></rss>');
fclose($file_rss);
?>