<?php
$atime = microtime(true);
$noct = true;

$document_root = __DIR__.'/..';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require_once($document_root.'/include/lib/phpmailer/src/PHPMailer.php');
require_once($document_root.'/include/lib/phpmailer/src/Exception.php');
require_once($document_root.'/include/lib/phpmailer/src/SMTP.php');
require_once($document_root.'/include/consts.php');

if(isset($simulate))
	echo "--simulate--\n";

$datejour = strftime('%d/%m/%Y');
$hrjr = strftime('%H:%M');

# Nettoyage de la table
$req = $bdd2->prepare('DELETE FROM `newsletter_mails` WHERE `expire`<?');
$req->execute(array(time()));

# Envoi des mails d'avertissement de fin d'abonnement
if(isset($debug)) {
	$req = $bdd2->prepare('SELECT * FROM `newsletter_mails` WHERE `confirm`=1 AND `expire`<=? AND `mail`=?');
	$req->execute(array(time()+172800, $debug));
	echo "--debug--\n";
}
else
{
	$req = $bdd2->prepare('SELECT * FROM `newsletter_mails` WHERE `confirm`=1 AND `expire`<=?');
	$req->execute(array(time()+172800));
	echo "--prod--\n";
}
while($data = $req->fetch()) {
	if(!isset($simulate)) {
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
		$mail->setFrom(SMTP_MAIL, SMTP_NAME);
		$mail->addReplyTo(SMTP_MAIL, SMTP_NAME);
		$mail->addAddress($data['mail']);
		$mail->Subject = $site_name.' : votre abonnement à l\'actu '.$site_name.' expire bientôt';
		$mail->CharSet = 'UTF-8';
		$mail->IsHTML(false);
		$mail->Body = 'Bonjour '.$data['mail'].",\n\nVotre abonnement à l'actu ".$site_name." expire le ".date('d/m/Y à H:i', $data['expire']).".\nCliquez sur le lien suivant pour le renouveler :\nhttps://www.progaccess.net/nlmod.php?id=".$data['hash']."\n\nCordialement,\n".$site_name;
		$mail->send();
	}
	echo $data['mail'];
}

# Sélection des mails
$r = '(freq_n=1';
if(localtime()[3] == 1)# premier jour du mois
	$r .= ' OR freq_n=5';
if(localtime()[6] == 1 and intval(date('W'))%2 == 0)# lundi et semaine paire
	$r .= ' OR freq_n=4';
if(localtime()[6] == 1)# lundi
	$r .= ' OR freq_n=3';
if(localtime()[7]%2 == 0)# jour pair sur l'année
	$r .= ' OR freq_n=2';
$r .= ')';

# Lister les catégories
$cat = array();
$req = $bdd->query('SELECT * FROM `softwares_categories`');
while($data = $req->fetch()) {$cat[$data['id']] = $data['name'];}

# Prendre des infos à envoyer
$sft = array();
$req = $bdd->prepare('SELECT * FROM `softwares` WHERE `date`>=? ORDER BY `date` DESC');
$req->execute(array(time()-2678400));# récents d'au plus un mois
while($data = $req->fetch()) {
	if(!isset($sft[$data['id']]))
		$sft[$data['id']] = array('category'=>$data['category'], 'hits'=>$data['hits'], 'date'=>$data['date'], 'author'=>$data['author'], 'name'=>$data['name'], 'description'=>$data['description']);
}

$req = $bdd->prepare('SELECT * FROM `softwares_files` WHERE `date`>=? ORDER BY `date` DESC');
$req->execute(array(time()-2678400));# récents d'au plus un mois
$files = array();
while($data = $req->fetch()) {
	$files[] = $data;
}

$subject = '📰 L\'actu '.$site_name.' du '.$datejour;
$message1 = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>'.$subject.'</title>
		<style type="text/css">
@font-face {font-family: Cantarell;src: url('.SITE_URL.'/css/Cantarell-Regular.otf);}
html, body {margin: 0;padding: 0;font-family: Cantarell;}
.software {border-left: 2px dashed black;padding-left: 10px;}
.software_title {margin-bottom: -8px;}
.software_date {color: #606060;margin-left: 15px;}
.software_hits, .software_category {color: #008000;}
</style>
	</head>
	<body>
		<div id="header">
					<h1>'.$subject.'</h1>
			<img id="logo" alt="Logo de '.$site_name.'" src="'.SITE_URL.'/images/nvda_logo.png">
		</div>
		<div id="content">
		<h2>Bonjour {{mail_user}},</h2>';
$message2 = '<hr><div role="contentinfo" aria-label="Informations sur l\'abonnement"><p>Votre abonnement expire le ';
$message3 = ', <a id="link" href="'.SITE_URL.'/nlmod.php?id=';
$message4 = '">cliquez ici pour le renouveler avant cette date</a>.</p>
			<p>Veuillez ne pas répondre, ce mail a été envoyé automatiquement, vous pouvez <a href="'.SITE_URL.'/inf.php">nous contacter ici</a></p>
			<p>Cordialement.<br>'.$site_name.'</p></div>
		</div>
	</body>
</html>';
$msgtxt1 = 'L\'actu '.$site_name.' du '.$datejour." (version texte)\nBonjour {{mail_user}},\n\n";
$msgtxt2 = 'Allez à l\'adresse ci-dessous pour gérer votre abonnement (à toute fin utile votre numéro d\'abonné est N{{idabonne}}). Vous serez automatiquement désinscrit de l\'actu le ';
$msgtxt3 = ".\n".SITE_URL."/nlmod.php?id=";
$msgtxt4 = "\n\nVeuillez ne pas répondre, ce mail a été envoyé automatiquement, cependant, vous pouvez nous contacter via notre formulaire de contact.\n\nCordialement.\n".$site_name;

# Envoi des mails
if(isset($debug)) {
	$req = $bdd2->prepare('SELECT * FROM `newsletter_mails` WHERE `confirm`=1 AND `mail`=?');
	$req->execute(array($debug));
	echo "--debug--\n";
}
else
{
	$req = $bdd2->prepare('SELECT * FROM `newsletter_mails` WHERE `confirm`=1 AND `notif_upd_n`=1 AND '.$r);
	$req->execute();
	echo "--prod--\n";
}
$nba = 0;
$nbt = 0;
$nbk = 0;
while($data = $req->fetch()) {
	$nba ++;
	$message = '';
	$msgtxt = '';
	$nbs = 0;# number of updated articles
	$nbf = 0;# number of updated files
	foreach($sft as $sw_id => $software) {
		if($software['date'] > $data['lastmail_n']) {
			$nbs ++;
			$message .= '<div class="software"><h3 class="software_title"><a href="'.SITE_URL.'/a'.$sw_id.'">'.$software['name'].'</a> (<a href="'.SITE_URL.'/c'.$software['category'].'">'.$cat[$software['category']].'</a>)</h3><p>'.str_replace('{{site}}', $site_name, $software['description']).'<br><span class="software_date">Mis à jour à '.date('H:i', $software['date']).' le '.date('d/m/Y', $software['date']).' par '.$software['author'].'</span><span class="software_hits">, '.$software['hits'].' visites</span></p><ul>';
			$msgtxt .= ' * '.$software['name'].' ('.$cat[$software['category']].") :\n".$software['description'].' ('.$software['hits'].' visites, mis à jour par '.$software['author'].' le '.date('d/m/Y à H:i', $software['date']).")\n";
			foreach($files as $file) {
				if($file['sw_id'] == $sw_id and $file['date'] > $data['lastmail_n']) {
					$nbf ++;
					$message .= '<li><a href="'.SITE_URL.'/dl/'.$file['id'].'">'.$file['title'].' (téléchargé '.$file['hits'].' fois)</a></li>';
					$msgtxt .= ' - '.$file['title'].', '.SITE_URL.'/dl/'.$file['id'].' ('.$file['hits']." téléchargements)\n";
				}
			}
			unset($file);
			$message .= '</ul></div>';
			$msgtxt .= "\n";
		}
	}
	unset($software);
	$message = $message1 . '<p>Depuis le '.date('d/m/Y', $data['lastmail_n']).', <strong>'.$nbs.'</strong> articles et <strong>'.$nbf.'</strong> fichiers ont été mis à jour.</p>' . $message;
	$msgtxt = $msgtxt1 . 'Depuis le '.date('d/m/Y', $data['lastmail_n']).", nous avons modifiés $nbs articles et $nbf fichiers.\n\n" . $msgtxt;
	echo $data['mail'];
	if($nbs > 0 or $nbf > 0) {
		echo ' send';
		$message .= $message2.date('d/m/Y, H:i', $data['expire']).$message3.$data['hash'].$message4;
		$msgtxt .= $msgtxt2.date('d/m/Y à H:i', $data['expire']).$msgtxt3.$data['hash'].$msgtxt4;
		
		$message = str_replace('{{mail}}', $data['mail'], str_replace('{{mail_user}}', ucfirst(explode('@', $data['mail'])[0]), str_replace('{{idabonne}}', $data['id'], $message)));
		$msgtxt = str_replace('{{mail}}', $data['mail'], str_replace('{{mail_user}}', ucfirst(explode('@', $data['mail'])[0]), str_replace('{{idabonne}}', $data['id'], $msgtxt)));
		$message = str_replace('{{site}}', $site_name, $message);
		$msgtxt = str_replace('{{site}}', $site_name, $msgtxt);
		
		if(isset($debug)) {
			print('<p>'.$msgtxt.'</p>');
		}
		
		if(!isset($simulate)) {
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->Host = SMTP_HOST;
			$mail->Port = SMTP_PORT;
			$mail->SMTPAuth = true;
			$mail->Username = SMTP_USERNAME;
			$mail->Password = SMTP_PSW;
			$mail->setFrom(SMTP_MAIL, SMTP_NAME);
			$mail->addReplyTo(SMTP_MAIL, SMTP_NAME);
			$mail->addAddress($data['mail']);
			$mail->Subject = $subject;
			$mail->CharSet = 'UTF-8';
			$mail->IsHTML(TRUE);
			$mail->Body = $message;
			$mail->AltBody = $msgtxt;
			$nbt ++;
			
			if($mail->send()) {
				echo ' OK';
				$req2 = $bdd2->prepare('UPDATE `newsletter_mails` SET `lastmail_n`=? WHERE id=? LIMIT 1');
				$req2->execute(array(time(), $data['id']));
				$nbk ++;
			}
			else
				echo ' Error:' . $mail->ErrorInfo;
		}
		echo "\n";
	}
}
$btime = microtime(true)-$atime;
echo $nba.' abonnés, '.$nbt.' envois, '.$nbk.' OK, '.$btime."s\n";
if($nbk > 0) {
		$message = "📤 Mail d'actu envoyé :\n-*".(intval($btime*1000)/1000)." secondes ;\n-*".$nbt." inscrits !\nConsultez vos mails 📥";
echo $message;
}
?>
