<?php
$document_root = __DIR__.'/..';
require_once($document_root.'/include/consts.php');
$reqpub=$bdd->prepare('SELECT * FROM `softwares` WHERE `publish_date`=?');
$reqpub->execute(array(date('Y-m-d', time())));
while($pub=$reqpub->fetch())
{
	$reqpub2 = $bdd->prepare('UPDATE softwares SET public=1 WHERE id=?');
	$reqpub2->execute(array($pub['id']));
}
$reqdel=$bdd->prepare('SELECT * FROM `softwares` WHERE `deletion_date`=?');
$reqdel->execute(array(date('Y-m-d', time())));
while($del=$reqdel->fetch())
{
	$reqf=$bdd->prepare('SELECT * FROM `softwares_files` WHERE `sw_id`=?');
	$reqf->execute(array($del['id']));
	while($fdel=$reqf->fetch())
	{
		unlink("../files/".$fdel['hash']);
		$reqfdel = $bdd->prepare('DELETE FROM softwares_files WHERE id=?');
		$reqfdel->execute(array($fdel['id']));
	}
	$reqdel2 = $bdd->prepare('DELETE FROM softwares WHERE id=?');
	$reqdel2->execute(array($del['id']));
}
?>