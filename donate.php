<?php set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$stats_page = 'donate';
$titre="Faire un Don à NVAccess"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<blockquote><p>Faire un don à NV Access permet de poursuivre notre mission qui est d'apporter un accès équitable aux technologies pour les mal et nonvoyants.
Ceci inclut le développement de la revue d'écran NVDA<br>
NVAccess étant une organisation caritative soumise aux lois australiennes, les dons ne sont pas assujettis aux taxes et reviennent entièrement à l'organisation et aux projets qu'elle soutient.</p>
<p>Vous pouvez donner à NVAccess par carte de crédit, chèque, transfert bancaire ou Compte PayPal. </p></blockquote>
<h2 id="CreditCardPayPalAccount">Carte de Crédit ou Compte PayPal</h2>
<p>En utilisant PayPal, vous pouvez faire un don sécurisé par carte de crédit ou par Compte PayPal. </p>
<form class="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" aria-label="Don unique">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="admin@nvaccess.org">
<input type="hidden" name="cpp_header_image" value="http://www.nvaccess.org/chrome/site/images/NVAccessVertical_200x140_op.jpg">
<input type="hidden" name="item_name" value="Single donation">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="no_shipping" value="1">
<table>
<tr>
<td><label for="single_amount">Faire un don unique de&nbsp;:</label></td>
<td>
<select id="single_amount" name="amount">
<option value="5.00">5,00€</option>
<option value="10.00" selected>10,00€</option>
<option value="15.00">15,00€</option>
<option value="20.00">20,00€</option>
<option value="50.00">50,00€</option>
<option value="100.00">100,00€</option>
<option value="200.00">200,00€</option>
<option value="500.00">500,00€</option>
<option value="1000.00">1000,00€</option>
</select>
</td>
</tr>
</table>
<input id="00000000" type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" name="submit" alt="don unique via PayPal">
<img id="00000001" alt="" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
</form>
<form action="https://www.paypal.com/fr/cgi-bin/webscr" method="post" aria-label="Don mensuel">
<input type="hidden" name="cmd" value="_xclick-subscriptions">
<input type="hidden" name="business" value="admin@nvaccess.org">
<input type="hidden" name="cpp_header_image" value="http://www.nvaccess.org/chrome/site/images/NVAccessVertical_200x140_op.jpg">
<input type="hidden" name="item_name" value="Monthly donation">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="src" value="1">
<input type="hidden" name="t3" value="M">
<input type="hidden" name="p3" value="1">
<table>
<tr>
<td><label for="subscribe_amount">Donner tous les mois&nbsp;:</label></td>
<td>
<select id="subscribe_amount" name="a3">
<option value="5.00">5,00€</option>
<option value="10.00" selected>10,00€</option>
<option value="15.00">15,00€</option>
<option value="20.00">20,00€</option>
<option value="50.00">50,00€</option>
<option value="100.00">100,00€</option>
<option value="200.00">200,00€</option>
<option value="500.00">500,00€</option>
<option value="1000.00">1000,00€</option>
</select>
</td>
</tr>
</table>
<input id="00000002" type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_subscribeCC_LG.gif" name="submit" alt="Don mensuel via PayPal">
<img id="00000003" alt="" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">
</form>
<h2 id="Cheque">Chèque</h2>
<blockquote><p>Veuillez noter que nous ne pouvons accepter que des chèques rédigés en dollars australiens.<br>
Les transferts bancaires électroniques sont généralement un meilleur choix pour les donnateurs internationaux.<br>
Vous pouvez faire un don par chèque comme suit&nbsp;:</p></blockquote>
<blockquote>
<p>
<strong>NV Access Limited<br>
19 Broadview Place<br>
Robina, Qld 4226<br>
Australia</strong>
</p>
</blockquote>
<h2 id="EFTbanktransfer">Transfert bancaire EFT</h2>
<p>Veuillez utiliser les informations suivantes pour transférer des dons à NV Access.<br>
Si possible, veuillez indiquer une référence/description du don.</p>
<ul>
<li>Banque&nbsp;: Westpac Banking Corporation (Robina branch), SHOP LC 5 06 ROBINA TOWN CENTRE ROBINA QLD 4226</li>
<li>Code SWIFT&nbsp;: WPACAU2S</li>
<li>BSB et n° de compte&nbsp;: 034279 - 349861</li>
<li>Nom du compte&nbsp;: NV Access Limited</li>
<li>Téléphone de la banque&nbsp;: 0061 292 939 270</li>
</ul>
<p>Cette page n'est qu'une traduction de la <a href="https://www.nvaccess.org/support-us/#donation-support">page de dons originale (en anglais) sur le site de NVAccess</a>. Cette page ne permet pas de donner à <?php print $site_name; ?> mais bien à NVAccess, développeurs de NVDA.</p>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>