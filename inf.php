﻿<?php set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$stats_page = 'inf';
$titre="Contacts et infos diverses"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<h2>Équipes</h2>
<p>Pendant plus de 10 ans, Michel Such s'est occupé seul de ce site, réalisant un travail sans faille.<br>
Pendant l'été 2018, Michel Such transmet le site à une partie de l'équipe déjà en charge du site <a href="https://www.progaccess.net">ProgAccess</a></p>
<h3>Équipe actuelle</h3>
<ul>
<?php
$req = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
$req->execute();
while($data = $req->fetch()) {
echo '<li>'.$data['name'].'</li>';
}
?>
<li>Petit plus&nbsp;: une grande partie de notre équipe est certifiée expert NVDA 2017 et/ou 2019, preuve d'une qualité d'expérience irréprochable.</li>
</ul>
<ul>
<li><a href="/contacter.php">Accéder au formulaire de contact</a></li>
<li><a href="<?php echo TWITTER_URL; ?>"><?php echo $site_name; ?> sur Twitter</a></li>
<li><a href="<?php echo MASTO_URL; ?>"><?php echo $site_name; ?> sur Mastodon</a></li>
</ul>
<h2>Rester en contact avec la communauté fondée autour de NVDA</h2>
<p>Il existe plusieurs listes de discussion permettant d'entrer en contact avec les utilisateurs et développeurs de NVDA.</p>
<h4>Listes francophones</h4>
<h5>La liste "ALLOS"</h5>
<p>ALLOS (Accessibilité des Logiciels Libres et Open Source) est un groupe de réflexion et d'échange d'expériences sur l'accessibilité de ces logiciels pour les personnes en situation de handicap visuel.<br>
ALLOS accueille volontiers toute personne en demande de conseil pour l'utilisation d'un logiciel libre dans le contexte d'un handicap visuel.<br>
Les administrateurs et modérateurs de ALLOS sont des membres de l'équipe de NVDA-FR.</p>
<ul>
<li><a href="https://groups.io/g/ALLOS/topics">Page d'accueil de ALLOS sur Groups.io</a></li>
<li><a href="mailto:ALLOS+subscribe@groups.io">Demander son inscription à ALLOS (inscriptions modérées)</a></li>
<li><a href="mailto:ALLOS+unsubscribe@groups.io">Se désinscrire de ALLOS</a></li>
</ul>
<h5>La liste scriptNVDA</h5>
<p>Apprentissage et découverte du langage de script pour NVDA en Français.</p>
<ul>
<li><a href="https://groups.io/g/script-NVDA/topics">Page d'accueil de la liste scriptNVDA sur Groups.io</a></li>
<li><a href="mailto:script-NVDA+subscribe@groups.io">S'inscrire à scriptNVDA</a></li>
<li><a href="mailto:script-NVDA+unsubscribe@groups.io">Se désinscrire de scriptNVDA</a></li>
</ul>
<h4>Listes anglophones</h4>
<h5>La liste NVDA</h5>
<p>La liste NVDA est la liste anglophone des utilisateurs de NVDA.</p>
<ul>
<li><a href="https://nvda.groups.io/g/nvda">Page d'accueil de la liste NVDA sur Groups.io</a></li>
</ul>
<h5>La liste NVDA-devel</h5>
<p>La liste NVDA-devel est la liste anglophone consacrée au développement de NVDA.</p>
<ul>
<li><a href="https://groups.io/g/nvda-devel">Page d'accueil de la liste NVDA-devel sur groups.io</a></li>
</ul>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>