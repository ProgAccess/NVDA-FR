<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Liste des fichiers - <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
<script src="/scripts/default.js"></script>
</head>
<body>
<h1>Liste des fichiers - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php';
$req = $bdd->prepare('
	SELECT `softwares_files`.`title`, `softwares_files`.`date`, `softwares_files`.`sw_id`, `softwares_files`.`name`, `softwares`.`repo`, `softwares`.`category`
	FROM `softwares`
	LEFT JOIN `softwares_files` ON `softwares`.`id`=`softwares_files`.`sw_id`');
$req->execute();
echo '<ul>'."\r\n";
while($data=$req->fetch())
{
	if(in_array($data['category'], CAT_MOREINFO))
	{
		echo '<li>'.$data['title'].', '.$data['repo'].', '.$data['name'].'</li>'."\r\n";
	}
}
?>
</body>
</html>