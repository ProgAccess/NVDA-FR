<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/include/MDConverter.php';
$time = time();
$addfile_hash = '';
$addfile_path = '';
function human_filesize($bytes, $decimals = 2) {
	$sz = ' kMGTP';
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

$sw_mode = null;
$sw_id = null;
if(isset($_GET['id'])) {$sw_id = $_GET['id'];$sw_mode = 1;}
elseif(isset($_GET['listfiles'])) {$sw_id = $_GET['listfiles'];$sw_mode = 2;}
elseif(isset($_GET['addfile'])) {$sw_id = $_GET['addfile'];$sw_mode = 3;}

// listing categories
$req = $bdd->query('SELECT * FROM `softwares_categories`');
$cat = array();
while($data = $req->fetch()) {$cat[$data['id']] = $data['name'];}
// modify software
if(isset($_GET['mod']) and isset($_POST['name']) and isset($_POST['category']) and isset($_POST['keywords']) and isset($_POST['description']) and isset($_POST['website']) and isset($_POST['text']) and isset($_POST['public']) and isset($_POST['repo']) and isset($_POST['datedel'])) {
	if($_POST['datedel']) $datesupr=$_POST['datedel']; else $datesupr="&&&";
	$req = $bdd->prepare('UPDATE softwares SET name=?, category=?, date=?, description=?, text=?, keywords=?, website=?, author=?, public=?, repo=?, deletion_date=? WHERE id=?');
	$req->execute(array($_POST['name'],$_POST['category'],$time,$_POST['description'],$_POST['text'],$_POST['keywords'],$_POST['website'],$nom,$_POST['public'],$_POST['repo'],$datesupr,$_GET['mod']));
	header('Location: sw_mod.php?list='.$_POST['category']);
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
	exit();
}
if(isset($_GET['rsw'])) {
	$req = $bdd->prepare('DELETE FROM `softwares` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['rsw']));
	include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
}
if(isset($_GET['modf2'])) {
	$req = $bdd->prepare('SELECT * FROM `softwares_files` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['modf2']));
	if($data = $req->fetch()) {
		if(isset($_FILES['file']) and $_FILES['file'] > 0 and $_FILES['file']['size'] <= 2147483648 and !empty($_FILES['file']['name'])) {
			$file = $_SERVER['DOCUMENT_ROOT'].'/files/'.$data['hash'];
			unlink($file);
			move_uploaded_file($_FILES['file']['tmp_name'], $file);
			$metadata = [];
			$reqSft = $bdd->prepare('SELECT * FROM `softwares` WHERE `id`=?');
			$reqSft->execute(array($data['sw_id']));
			if ($sft = $reqSft->fetch()) {
			if (in_array($sft['category'], CAT_MOREINFO))
			{
				include_once($_SERVER['DOCUMENT_ROOT'].'/include/addon_info.php');
				$metadata = json_encode(addon_info($file));
			}
			}
			$req = $bdd->prepare('UPDATE `softwares_files` SET `name`=?, `filetype`=?, `title`=?, `date`=?, `filesize`=?, `label`=?, `md5`=?, `sha1`=?, `metadata`=? WHERE `id`=? LIMIT 1');
			$req->execute(array($_FILES['file']['name'], $_FILES['file']['type'], $_POST['title'], time(), $_FILES['file']['size'], $_POST['label'], md5_file($file), sha1_file($file), $metadata, $_GET['modf2']));
		}
		else {
			$req = $bdd->prepare('UPDATE `softwares_files` SET `name`=? , `title`=? , `label`=?, `date`=? WHERE `id`=? LIMIT 1');
			$req->execute(array($_POST['name'], $_POST['title'], $_POST['label'], time(), $_GET['modf2']));
		}
		
		header('Location: sw_mod.php?listfiles='.$data['sw_id']);
		$req = $bdd->prepare('UPDATE `softwares` SET `date`=?, `author`=? WHERE `id`=? LIMIT 1');
		$req->execute(array(time(), $nom, $data['sw_id']));
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
		$somsg = $_POST['title'].' : '.SITE_URL.'/dl/'.(!empty($_POST['label']) ? $_POST['label']:$data['id']).' '.$nom;
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
		send_twitter($somsg);
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
		send_mastodon($somsg);
		exit();
	}
	// OLD
	/*$req = $bdd->prepare('UPDATE `softwares_files` SET `name`=? , `title`=? , `label`=?, `date`=? WHERE `id`=? LIMIT 1');
	$req->execute(array($_POST['name'], $_POST['title'], $_POST['label'], time(), $_GET['modf2']));
	$req = $bdd->prepare('SELECT `sw_id` FROM `softwares_files` WHERE `id`=?');
	$req->execute(array($_GET['modf2']));
	if($data = $req->fetch())
		header('Location: sw_mod.php?listfiles='.$data['sw_id']);
	else
		header('Location: sw_mod.php');
	include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
	exit();*/
}
if(isset($_GET['modm2'])) {
	$req = $bdd->prepare('UPDATE `softwares_mirrors` SET `title`=? , `links`=? , `label`=?, `date`=? WHERE `id`=? LIMIT 1');
	$req->execute(array($_POST['title'], $_POST['urls'], $_POST['label'], time(), $_GET['modm2']));
	header('Location: sw_mod.php?listfiles='.$_GET['modm2']);
	include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
	exit();
}
/*if(isset($_GET['vfile'])) {
	$req1 = $bdd->prepare('SELECT `id`, `sw_id`, `title`, `label` FROM `softwares_files` WHERE `hash`=? LIMIT 1');
	$req1->execute(array($_GET['vfile']));
	if($data = $req1->fetch()) {
		$file = $_SERVER['DOCUMENT_ROOT'].'/files/'.$_GET['vfile'];
		if(file_exists($file)) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$req2 = $bdd->prepare('UPDATE `softwares_files` SET `filetype`=?, `date`=?, `filesize`=?, `md5`=?, `sha1`=? WHERE `id`=? LIMIT 1');
			$req2->execute(array(finfo_file($finfo,$file), time(), filesize($file), md5_file($file), sha1_file($file), $data['id']));
			finfo_close($finfo);
			$somsg = $data['title'].' : '.SITE_URL.'/dl/'.(!empty($data['label']) ? $data['label']:$data['id']).' '.$nom;
			include_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
			send_twitter($somsg);
			require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
			send_mastodon($somsg);
			header('Location: sw_mod.php?listfiles='.$data['sw_id']);
								include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
		}
		else {
			$addfile_hash = $_GET['vfile'];
			$addfile_path = $file;
		}
		header('Location: sw_mod.php?listfiles='.$data['sw_id']);
	}
	$req1->closeCursor();
}*/
if(isset($_GET['vfile'])) {
	$req1 = $bdd->prepare('SELECT `id`, `sw_id`, `title`, `label` FROM `softwares_files` WHERE `hash`=? LIMIT 1');
	$req1->execute(array($_GET['vfile']));
	if($data = $req1->fetch()) {
		$file = $_SERVER['DOCUMENT_ROOT'].'/files/'.$_GET['vfile'];
		if(file_exists($file)) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$metadata = [];
			$reqSft = $bdd->prepare('SELECT * FROM `softwares` WHERE `id`=?');
			$reqSft->execute(array($data['sw_id']));
			if ($sft = $reqSft->fetch()) {
			if (in_array($sft['category'], CAT_MOREINFO))
			{
				include_once($_SERVER['DOCUMENT_ROOT'].'/include/addon_info.php');
				$metadata = json_encode(addon_info($file));
			}
			}
			$req2 = $bdd->prepare('UPDATE `softwares_files` SET `filetype`=?, `date`=?, `filesize`=?, `md5`=?, `sha1`=?, `metadata`=? WHERE `id`=? LIMIT 1');
			$req2->execute(array(finfo_file($finfo,$file), time(), filesize($file), md5_file($file), sha1_file($file), $metadata, $data['id']));
			finfo_close($finfo);
			$somsg = $data['title'].' : '.SITE_URL.'/dl/'.$label.' '.$nom;
			include_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
			send_twitter($somsg);
			require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
			send_mastodon($somsg);
			include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
			header('Location: sw_mod.php?listfiles='.$data['sw_id']);
			exit();
		}
		$addfile_hash = $_GET['vfile'];
		$addfile_path = $file;
	}
	else {
		header('Location: sw_mod.php');
		exit();
	}
}
if(isset($_GET['addmirror']) and isset($_POST['title']) and isset($_POST['urls'])) {
	$req1 = $bdd->prepare('SELECT `id` FROM `softwares` WHERE `id`=? LIMIT 1');
	$req1->execute(array($_GET['addmirror']));
	if($req1->fetch()) {
		if(isset($_POST['label']) and !empty($_POST['label'])) {
			$label = htmlspecialchars($_POST['label']);
			$req = $bdd->prepare('UPDATE `softwares_mirrors` SET `label`="" WHERE `label`=? LIMIT 1');
			$req->execute(array($label));
		}
		else
			$label = '';
		$req2 = $bdd->prepare('INSERT INTO `softwares_mirrors` (`sw_id`,`links`,`title`,`date`,`label`) VALUES (?,?,?,?,?)');
		$req2->execute(array($_GET['addmirror'], $_POST['urls'], $_POST['title'], time(), $label));
	}
	$req418 = $bdd->prepare('SELECT * FROM softwares_mirrors WHERE id=?');
	$req418->execute(array($_GET['addmirror']));
if($data37 = $req418->fetch()) {
header('Location: sw_mod.php?listfiles='.$data37['sw_id']);
}
}
/*if(isset($_GET['upload']) and isset($_POST['title'])) {
	$sendreq = 0;
	$req1 = $bdd->prepare('SELECT `id` FROM `softwares` WHERE `id`=? LIMIT 1');
	$req1->execute(array($_GET['upload']));
	if($req1->fetch()) {
		$hash = base_convert(sha1($_POST['name']).sha1(strval(time())), 16, 36);
		$file = $_SERVER['DOCUMENT_ROOT'].'/files/'.$hash;
		if(!file_exists($file)) {
			if(isset($_FILES['file']) and $_FILES['file'] > 0 and $_FILES['file']['size'] <= 2147483648 and (!isset($_POST['nofile']) or (isset($_POST['nofile']) and $_POST['nofile'] != 'on'))) {
				move_uploaded_file($_FILES['file']['tmp_name'], $file);
				$sendreq = 1;
			}
			elseif(isset($_POST['nofile']) and $_POST['nofile'] == 'on' and isset($_POST['name'])) {
				$sendreq = 2;
				$addfile_hash = $hash;
				$addfile_path = $file;
			}
		}
	}
	if(isset($_POST['label']) and !empty($_POST['label'])) {
		$label = htmlspecialchars($_POST['label']);
		$req = $bdd->prepare('UPDATE `softwares_files` SET `label`="" WHERE `label`=? LIMIT 1');
		$req->execute(array($label));
	}
	else
		$label = '';
	$req1->closeCursor();
	if($sendreq != 0) {
		$req = $bdd->prepare('UPDATE `softwares` SET `date`=? WHERE `id`=? LIMIT 1');
		$req->execute(array(time(), $_GET['upload']));
	}
	if($sendreq == 1) {
		$req = $bdd->prepare('INSERT INTO softwares_files(sw_id,name,hash,filetype,title,date,filesize,label,`md5`,`sha1`) VALUES(?,?,?,?,?,?,?,?,?)');
		$req->execute(array($_GET['upload'],$_FILES['file']['name'],$hash,$_FILES['file']['type'],$_POST['title'],time(),$_FILES['file']['size'],$label,md5_file($file),sha1_file($file)));
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
		$somsg = $_POST['title'].' :';
		if(!empty($label))
			$somsg .= ' '.SITE_URL.'/dl/'.$label.' '.$nom;
		include_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
		send_twitter($somsg);
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
		send_mastodon($somsg);
	}
	else if($sendreq == 2) {
		$req = $bdd->prepare('INSERT INTO softwares_files(sw_id,name,hash,title,label) VALUES(?,?,?,?,?)');
		$req->execute(array($_GET['upload'],$_POST['name'],$hash,$_POST['title'],$label));
	}
	if($sendreq != 2) {
		header('Location: sw_mod.php?listfiles='.$_GET['upload']);
		exit();
	}
}*/
if(isset($_GET['upload']) and isset($_POST['title'])) {
	$sendreq = 0;
	$req1 = $bdd->prepare('SELECT `id` FROM `softwares` WHERE `id`=? LIMIT 1');
	$req1->execute(array($_GET['upload']));
	if($req1->fetch()) {
		$hash = base_convert(sha1($_POST['name'].time()), 16, 36);
		$file = $_SERVER['DOCUMENT_ROOT'].'/files/'.$hash;
		if(!file_exists($file)) {
			if(isset($_FILES['file']) and $_FILES['file'] > 0 and $_FILES['file']['size'] <= 2147483648 and (!isset($_POST['nofile']) or (isset($_POST['nofile']) and $_POST['nofile'] != 'on'))) {
				move_uploaded_file($_FILES['file']['tmp_name'], $file);
				$sendreq = 1;
			}
			elseif(isset($_POST['nofile']) and $_POST['nofile'] == 'on' and isset($_POST['name'])) {
				$sendreq = 2;
				$addfile_hash = $hash;
				$addfile_path = $file;
			}
		}
	}
	if(isset($_POST['label']) and !empty($_POST['label'])) {
		$label = htmlspecialchars($_POST['label']);
		$req = $bdd->prepare('UPDATE `softwares_files` SET `label`="" WHERE `label`=? LIMIT 1');
		$req->execute(array($label));
	}
	else
		$label = '';
	$req1->closeCursor();
	if($sendreq != 0) {
		$req = $bdd->prepare('UPDATE `softwares` SET `date`=? WHERE `id`=? LIMIT 1');
		$req->execute(array(time(), $_GET['upload']));
	}
	if($sendreq == 1) {
			$metadata = [];
			if (in_array($_GET['upload'], CAT_MOREINFO))
			{
				include_once($_SERVER['DOCUMENT_ROOT'].'/include/addon_info.php');
				$metadata = json_encode(addon_info($file));
			}
		$req = $bdd->prepare('INSERT INTO softwares_files(sw_id,name,hash,filetype,title,date,filesize,label,`md5`,`sha1`,`metadata`) VALUES(?,?,?,?,?,?,?,?,?,?,?)');
		$req->execute(array($_GET['upload'],$_FILES['file']['name'],$hash,$_FILES['file']['type'],$_POST['title'],time(),$_FILES['file']['size'],$label,md5_file($file),sha1_file($file),$metadata));
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
		$somsg = $_POST['title'].' :';
		if(!empty($label))
			$somsg .= ' '.SITE_URL.'/dl/'.$label.' '.$nom;
		include_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
		send_twitter($somsg);
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
		send_mastodon($somsg);
	}
	else if($sendreq == 2) {
		$req = $bdd->prepare('INSERT INTO softwares_files(sw_id,name,hash,title,label) VALUES(?,?,?,?,?)');
		$req->execute(array($_GET['upload'],$_POST['name'],$hash,$_POST['title'],$label));
	}
	if($sendreq != 2) {
		header('Location: sw_mod.php?listfiles='.$_GET['upload']);
		exit();
	}
}

if(isset($_GET['rfiles'])) {
	$req1 = $bdd->prepare('SELECT id, hash FROM softwares_files WHERE sw_id=?');
	$req1->execute(array($_GET['rfiles']));
	while($data = $req1->fetch()) {
		if(isset($_GET['rfile'.$data['id']])) {
			$req2 = $bdd->prepare('DELETE FROM softwares_files WHERE id=? LIMIT 1');
			$req2->execute(array($data['id']));
			header('Location: sw_mod.php?addfile='.$_GET['rfiles']);
			unlink($_SERVER['DOCUMENT_ROOT'].'/files/'.$data['hash']);
		}
	}
	$req1 = $bdd->prepare('SELECT `id` FROM `softwares_mirrors` WHERE `sw_id`=?');
	$req1->execute(array($_GET['rfiles']));
	while($data = $req1->fetch()) {
		if(isset($_GET['rmir'.$data['id']])) {
			$req2 = $bdd->prepare('DELETE FROM `softwares_mirrors` WHERE `id`=? LIMIT 1');
			$req2->execute(array($data['id']));
			header('Location: sw_mod.php?addfile='.$_GET['rfiles']);
		}
	}
	$req1->closeCursor();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
<title><?php
if($sw_id != null and $sw_mode != null) {
	$req = $bdd->prepare('SELECT `name` FROM `softwares` WHERE `id`=? LIMIT 1');
	$req->execute(array($sw_id));
	if($sw_mode == 1) echo 'Modifier ';
	if($sw_mode == 2) echo 'Fichiers de ';
	if($sw_mode == 3) echo 'Nouveau fichier à ';
	if($data = $req->fetch())
		echo $data['name'];
	else
		echo 'un article';
}
else
	echo 'Modifier un article';			
?> &#8211; admin <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
<script type="text/javascript" src="/scripts/default.js"></script>
	</head>
	<body>
<h1>Modifier logiciel - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php'; ?>
<a href="sw_add.php">Ajouter un article</a>
<?php if(empty($_GET)) {
		echo '<ul title="Lister les articles de&nbsp;:">';
		$req2 = $bdd->query('SELECT * FROM softwares_categories ORDER BY name ASC');
while($data2 = $req2->fetch()) {
		echo '<li><a href="sw_mod.php?list='.$data2['id'].'">'.$data2['name'].'</a></li>';
}
$req2->closeCursor();
echo '</ul>';
} else {
echo '<br><a href="sw_mod.php">Retourner au listage des catégories</a>';
} ?>
		<?php if($addfile_hash != '' and $addfile_path != '') {
	echo '<p>L\'ajout du fichier n\'est pas terminé.<br>Veuillez envoyer le fichier à cet emplacement&nbsp;:<br><strong>'.$addfile_path.'</strong><br>Son nom doit être <br><em>'.$addfile_hash.'</em><br> sans extension.<br>Une fois ceci fait, suivez ce lien&nbsp;:<br><a href="?vfile='.$addfile_hash.'">Vérifier le fichier</a></p>';
}

if(isset($_GET['list'])) { ?><table border="1">
			<thead><tr><th>Nom</th><th>Catégorie</th><th>État</th><th>Dernière modification</th></tr></thead>
			<tbody>
<?php
// listing softwares
	$req = $bdd->prepare('SELECT * FROM softwares WHERE category=? ORDER BY name ASC');
	$req->execute(array($_GET['list']));
while($data = $req->fetch()) {
	switch($data['public']) {
		case '0': $statepublic = 'Brouillon'; break;
		case '1': $statepublic = 'Public'; break;
	}
	echo '<tr><td>';
	if($data['public'] == '0') { echo '<details><summary><h5>'.$data['name'].'</h5></summary>'; } else { echo '<details><summary><h6>'.$data['name'].'</h6></summary>'; }
	echo '<ul role="menu"><li role="menuitem"><a href="?id='.$data['id'].'">Éditer</a></li><li role="menuitem"><a href="?listfiles='.$data['id'].'">Afficher les fichiers</a></li><li role="menuitem"><a href="?rsw='.$data['id'].'">Supprimer</a></li></ul></details>';
	if($data['public'] == '0') { echo '</h5>'; } else { echo '</h6>'; }
	echo '</td><td>'.$cat[$data['category']].'</td><td>'.$statepublic;
	if($data['public'] == '0') { echo ' (<a href="/a'.$data['id'].'">Consulter le rendu de l\'article</a>)';
	}
	echo '</td><td>'.date('d/m/Y H:i:s',$data['date']).' par '.$data['author'].'</td></tr>';
}
?>
			</tbody>
		</table><?php }
if(isset($_GET['listfiles'])) {
	$req1 = $bdd->prepare('SELECT id,name,category,repo FROM softwares WHERE id=? ORDER BY date ASC');
	$req1->execute(array($_GET['listfiles']));
	if($data1 = $req1->fetch()) {
	?>
		<p>Liste des fichiers de&nbsp;: <a href="?id=<?php echo $data1['id']; ?>"><?php echo $data1['name']; ?></a>&nbsp;:</p><ul><?php if(!strstr($_SERVER['HTTP_HOST'], 'dev.')) { ?><li><a href="?addfile=<?php echo $data1['id']; ?>">Ajouter un fichier</a></li><?php } ?><li><a href="?list=<?php echo $data1['category']; ?>"><?php echo $cat[$data1['category']]; ?></a></li><?php if(in_array($data1['category'], CAT_MOREINFO) && $data1['repo'] != '') echo '<li><a target="_blank" rel="noopener" href="'.$data1['repo'].'">Site ou dépôt officiel</a></li>'; ?></ul>
		<form action="#" method="get">
			<input type="hidden" name="rfiles" value="<?php echo $data1['id']; ?>" autocomplete="off">
			<table border="1">
				<thead><tr><th>Nom</th><th>Titre</th><th>Label</th><th>Type</th><th>Modifié le</th><th>Taille</th><th>Supprimer</th></tr></thead>
				<tbody>
	<?php	$req2 = $bdd->prepare('SELECT * FROM softwares_files WHERE sw_id=? ORDER BY date ASC');
			$req2->execute(array($_GET['listfiles']));
			while($data2 = $req2->fetch()) {
				echo '<tr><td><a href="?modf='.$data2['id'].'">'.$data2['name'].'</a></td><td>'.$data2['title'].'</td><td><a href="/dl/'.$data2['label'].'">'.$data2['label'].'</a></td><td>'.$data2['filetype'].'</td><td>'.date('d/m/Y H:i:s',$data2['date']).'</td><td>'.human_filesize($data2['filesize']).'o</td><td><input type="checkbox" name="rfile'.$data2['id'].'" autocomplete="off"></td></tr>';
			} $req2->closeCursor(); ?></tbody>
			</table>
			<table border="1">
				<thead><tr><th>Titre</th><th>Adresses</th><th>Label</th><th>Modifié le</th><th>Supprimer</th></tr></thead>
				<tbody>
	<?php	$req2 = $bdd->prepare('SELECT * FROM `softwares_mirrors` WHERE `sw_id`=? ORDER BY `date` ASC');
			$req2->execute(array($_GET['listfiles']));
			while($data2 = $req2->fetch()) {
				echo '<tr><td><a href="?modm='.$data2['id'].'">'.$data2['title'].'</a></td><td><textarea name="lmir'.$data2['id'].'" readonly>'.htmlentities($data2['links']).'</textarea></td><td><a href="/r?m&p='.$data2['label'].'">'.$data2['label'].'</a></td><td>'.date('d/m/Y H:i:s',$data2['date']).'</td><td><input type="checkbox" name="rmir'.$data2['id'].'" autocomplete="off"></td></tr>';
			} $req2->closeCursor(); ?></tbody>
			</table>
			<input type="submit" value="Supprimer">
		</form><?php }
	$req1->closeCursor();
}

if(isset($_GET['id'])) {
	$req = $bdd->prepare('SELECT * FROM softwares WHERE id=?');
	$req->execute(array($_GET['id']));
	if($data = $req->fetch()) {
?>
		<form action="?mod=<?php echo $_GET['id']; ?>" method="post">
			<label for="f_mod_name">Nom&nbsp;:</label><input type="text" name="name" value="<?php echo $data['name']; ?>" id="f_mod_name" maxlength="255" required><br>
			<label for="f_mod_category">Catégorie&nbsp;:</label><select name="category" id="f_mod_category" onchange="showother()"><?php
$rq2 = $bdd->query('SELECT * FROM softwares_categories ORDER BY name ASC');
while($dat2 = $rq2->fetch()) {
	echo '<option value="'.$dat2['id'].'"';
	if($dat2['id'] == $data['category']) echo ' selected';
	echo '>'.$dat2['name'].'</option>';
}
$rq2->closeCursor()
?></select><br>
			<label for="f_mod_keywords">Mots clés&nbsp;:</label><input type="text" name="keywords" value="<?php echo $data['keywords']; ?>" id="f_mod_keywords" maxlength="255" required><br>
			<label for="f_mod_description">Description courte&nbsp;:</label><input type="text" name="description" value="<?php echo $data['description']; ?>" id="f_mod_description" maxlength="1024" required><br>
			<label for="f_website" id="f_website02">Type d'extension&nbsp;:</label><select id="f_website" name="website">
			<option value="Applicative" <?php if($data['website'] == 'Applicative') { echo 'selected'; } ?>>Applicative</option>
			<option value="Globale"<?php if($data['website'] == 'Globale') { echo 'selected'; } ?>>Globale</option>
			<option value="Mixt"<?php if($data['website'] == 'Mixt') { echo 'selected'; } ?>>Mixt</option>
			</select><br>
			<label for="f_mod_repo" id="f_mod_repo02">Adresse du site ou du dépôt&nbsp;:</label><input type="text" name="repo" value="<?php echo $data['repo']; ?>" id="f_mod_repo" maxlength="255"><br>
				<script type="text/javascript">
function showother() {
	if(document.getElementById("f_mod_category").value == "1") {
		document.getElementById("f_website02").style = "";
		document.getElementById("f_website").style = "";
		document.getElementById("f_mod_repo02").style = "";
		document.getElementById("f_mod_repo").style = "";
	} else {
		document.getElementById("f_website02").style = "display: none;";
		document.getElementById("f_website").style = "display: none;";
		document.getElementById("f_mod_repo02").style = "display: none;";
		document.getElementById("f_mod_repo").style = "display: none;";
	}
}
showother();
</script>
			<label for="f_mod_text">Texte long (HTML ou MarkDown)&nbsp;:</label><br>
			<textarea name="text" id="f_mod_text" maxlength="20000" rows="20" cols="500" required><?php echo convertToMD($data['text']); ?></textarea><br>
			<label for="f_mod_public">État de l'article&nbsp;:</label><select id="f_mod_public" name="public">
			<option value="0" <?php if($data['public'] == '0') { echo 'selected'; } ?>>Brouillon (accessible uniquement via l'interface d'admin)</option>
			<option value="1" <?php if($data['public'] == '1') { echo 'selected'; } ?>>Public (affiché dans la liste des articles)</option>
			</select>
		<br>
		<label for="l_datedel">Date de suppression&nbsp;:</label>
		<input type="date" id="l_datedel" name="datedel" value="<?php echo $data['deletion_date']; ?>" min="<?php echo date('Y-m-d', time()); ?>"><br>
			<input type="submit" value="Modifier">
		</form><?php }$req->closeCursor();}
if(isset($_GET['addfile'])) {
	$req = $bdd->prepare('SELECT * FROM softwares WHERE id=? ORDER BY name ASC');
	$req->execute(array($_GET['addfile']));
	if($data = $req->fetch()) { ?>
		<p>Ajouter un fichier pour <em><?php echo $data['name']; ?></em></p>
		<form action="?upload=<?php echo $_GET['addfile']; ?>" method="post" enctype="multipart/form-data">
			<fieldset><legend>Ajouter un fichier</legend>
				<label for="f_addfile_title">Titre du fichier&nbsp;:</label>
				<input type="text" name="title" id="f_addfile_title"><br>
				<label for="f_addfile_nofile">Envoi hors-formulaire (si suppérieur à 2GO)&nbsp;:</label>
				<input type="checkbox" id="f_addfile_nofile" name="nofile" title="Cochez cette case si le fichier fait plus de 2Mo ou si votre connexion est très lente" onchange="showother2()">
				<p>Ne remplissez le champ "nom" suivant uniquement si vous avez coché la case précédente. Mettez-y le nom original du fichier, avec extension, par exemple "toto_pro_v7.7_installer.exe".</p>
				<label for="f_addfile_name" id="js212">Nom du fichier&nbsp;:</label>
				<input type="text" name="name" id="f_addfile_name">
												<script type="text/javascript">
function showother2() {
	if(document.getElementById("f_addfile_nofile").checked == true) {
		document.getElementById("js212").style = "";
		document.getElementById("f_addfile_name").style = "";
	} else {
		document.getElementById("js212").style = "display: none;";
		document.getElementById("f_addfile_name").style = "display: none;";
	}
}
showother2();
</script>
				<label for="f_addfile_label">Label&nbsp;:</label>
				<input type="text" name="label" id="f_addfile_label" maxlength="16">
				<p>Si la taille du fichier est supérieure à 2GO, n'utilisez pas le champ d'envoi de fichier. Dans ce cas vous devrez envoyer un fichier en dehors du formulaire après avoir validé celui-ci.</p>
				<label for="f_addfile_file">Fichier&nbsp;:</label>
				<input type="file" name="file" id="f_addfile_file"><br>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
		<form action="?addmirror=<?php echo $_GET['addfile']; ?>" method="post">
			<fieldset><legend>Ajouter un mirroir</legend>
				<label for="f_addmirror_title">Titre du fichier&nbsp;:</label>
				<input type="text" name="title" id="f_addmirror_title"><br>
				<label for="f_addmirror_urls">URLs des mirroirs&nbsp;:</label><br>
				<textarea name="urls" id="f_addmirror_urls" style="width: 100%;"></textarea>
				<p>Exemple&nbsp;: [["ZettaScript","https://zettascript.org/fichier.tar.gz"],["CommentÇaMarche","https://commentcamarche.net/download/fichier"]]</p>
				<label for="f_addmirror_label">Label&nbsp;:</label>
				<input type="text" name="label" id="f_addmirror_label" maxlength="16"><br>
				<input type="submit" value="Ajouter">
			</fieldset>
		</form>
<?php }$req->closeCursor();}
if(isset($_GET['modf'])) {
	$req = $bdd->prepare('SELECT * FROM softwares_files WHERE id=?');
	$req->execute(array($_GET['modf']));
	if($data = $req->fetch()) { ?>
		<form action="?modf2=<?php echo $data['id']; ?>" method="post" enctype="multipart/form-data">
			<h2>Modifier un fichier</h2>
			<fieldset>
				<legend>Métadonnées</legend>
				<label for="f_modf_title">Titre du fichier&nbsp;:</label>
				<input type="text" name="title" id="f_modf_title" value="<?php echo $data['title']; ?>"><br>
				<label for="f_modf_name" style="display: none;">Nom du fichier&nbsp;:</label>
				<input type="text" name="name" id="f_modf_name" value="<?php echo $data['name']; ?>" style="display: none;">
				<label for="f_modf_label">Label&nbsp;:</label>
				<input type="text" name="label" id="f_modf_label" value="<?php echo $data['label']; ?>" maxlength="16" readonly=<?php (!empty($data['label'])?true:false); ?>>
				<?php if(!empty($data['label'])) echo '<p>Le label de ce fichier est déjà renseigné, pour le modifier, supprimez ce fichier et ajoutez en un nouveau.</p>'; ?>
			</fieldset>
			<fieldset>
				<legend>Remplacer le fichier</legend>
				<label for="f_modf_file">Fichier&nbsp;:</label>
				<input type="file" name="file" id="f_modf_file">
			</fieldset>
			<input type="submit" value="Modifier">
		</form>
<?php }$req->closeCursor();}
if(isset($_GET['modm'])) {
	$req = $bdd->prepare('SELECT * FROM `softwares_mirrors` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['modm']));
	if($data = $req->fetch()) { ?>
		<form action="?modm2=<?php echo $data['id']; ?>" method="post">
			<h2>Modifier un mirroir</h2>
			<label for="f_modf_title">Titre du fichier&nbsp;:</label>
			<input type="text" name="title" id="f_modm_title" value="<?php echo $data['title']; ?>"><br>
			<label for="f_modm_urls">URLs des mirroirs&nbsp;:</label><br>
			<textarea name="urls" id="f_modm_urls"><?php echo htmlentities($data['links']); ?></textarea><br>
			<label for="f_modf_label">Label&nbsp;:</label>
			<input type="text" name="label" id="f_modm_label" maxlength="16" value="<?php echo $data['label']; ?>"><br>
			<input type="submit" value="Modifier">
		</form>
<?php }$req->closeCursor();} ?>
	</body>
</html>