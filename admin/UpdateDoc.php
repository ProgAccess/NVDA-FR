<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
if(isset($_GET['upload']))
{
	$file = $_SERVER['DOCUMENT_ROOT'].'/doc/'.$_FILES['file']['name'];
	if(isset($_FILES['file']))
		if(move_uploaded_file($_FILES['file']['tmp_name'], $file)) {
			$log='Fichier téléversé';
			if($_FILES['file']['name'] == 'changes.html')
				$filetitle='Quoi de neuf dans NVDA';
			if($_FILES['file']['name'] == 'keyCommands.html')
				$filetitle='Résumé des commandes de NVDA';
			if($_FILES['file']['name'] == 'userGuide.html')
				$filetitle='Guide de l\'utilisateur de NVDA';
			if(isset($filetitle)) {
				$somsg = 'Le fichier de documentation "'.$filetitle.'" a été mis à jour. '.SITE_URL.'/doc/'.$_FILES['file']['name'].'. '.$nom;
				require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
				send_twitter($somsg);
				require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
				send_mastodon($somsg);
			}
		}
		else
			print_r($_FILES);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Mise à jour de la documentation - <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
</head>
<body>
<h1>Mise à jour de la documentation - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php';
if(!empty($log))
	print('<p role="alert">'.$log.'</p>');
?>
<p>Téléverser un des fichiers de documentation via le formulaire ci-dessous pour le mettre à jour sur le site.<br>
Utilisez la liste des fichiers affichée sous le formulaire pour vérifier la date de modification des fichiers.</p>
<form action="?upload" method="post" enctype="multipart/form-data">
<label for="l_file">Fichier&nbsp;:</label>
<input type="file" name="file" id="l_file" required><br>
<input type="submit" value="Téléverser">
</form>
<h2>Fichiers actuellement disponibles</h2>
<?php
$scandir = scandir("../doc");
echo '<ul>';
foreach($scandir as $fichier)
{
	if(preg_match("#\.(html)$#",strtolower($fichier)))
	{
		echo '<li><a href="../doc/'.$fichier.'">'.$fichier.'</a>&nbsp;: modifié le '.strftime('%A %d/%m/%Y', filemtime("../doc/".$fichier)).'</li>';
	}
}
?>
</body>
</html>