<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Liste des mails de l'équipe</title>
</head>
<body>
<h1>Liste des mails de l'équipe</h1>
<?php
require_once('include/dbconnect.php');
$reqGAddr = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
$reqGAddr->execute();
echo '<ul>';
while($data = $reqGAddr->fetch()) {
	$reqGAddr2 = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
	$reqGAddr2->execute(array($data['account_id']));
	while($data2 = $reqGAddr2->fetch()) {
		echo '<li>Nom&nbsp;: '.$data['short_name'].', Email&nbsp;: '.$data2['email'].'</li>';
	}
}
echo '</ul>';
?>
</body>
</html>