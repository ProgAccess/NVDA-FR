<?php $logonly = true;
$adminonly=true;
$justnvda = true;
require $_SERVER['DOCUMENT_ROOT'].'/include/log.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/PHPMailer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/Exception.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/SMTP.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/MDConverter.php');
if(isset($_GET['archive'])) {
	$req = $bdd->prepare('UPDATE `tickets` SET `status`=3 WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['archive']));
	$log='Ticket archivé';
}
if(isset($_GET['waiting'])) {
	$req = $bdd->prepare('UPDATE `tickets` SET `status`=2 WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['waiting']));
	$log='Ticket marqué comme lu';
}
if(isset($_GET['close']) and isset($_POST['clo']) and $_POST['clo'] == 'FERMER') {
	$req = $bdd->prepare('SELECT * FROM `tickets` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['close']));
	if($data = $req->fetch()) {
		$body = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Ticket '.$site_name.' fermé : "'.htmlspecialchars($data['subject']).'"</title>
		<style type="text/css">#response{border-left:1px solid #0080FF;margin-left:8px;padding: 8px;}</style>
	</head>
	<body>
<h1>'.$site_name.'</h1>
		<p>'.$nom.' vient de fermer votre ticket <i>'.htmlspecialchars($data['subject']).'</i> sur '.$site_name.'.</p>
		<p>Vous pouvez réouvrir ce ticket dans les 24 heures simplement en y répondant, passez cette date la discussion sur ce ticket s\'arrêtera définitivement et vous devrez réouvrir un ticket pour continuer à discuter avec nous.</p>
		<hr>
		<p>Merci de ne pas répondre à cet e-mail. Pour nous envoyer votre réponse, veuillez utiliser le lien ci-dessous.<br>
			<a href="'.SITE_URL.'/contacter.php?reply='.$data['id'].'&h='.$data['hash'].'">'.SITE_URL.'/contacter.php?reply='.$data['id'].'&h='.$data['hash'].'</a></p>
	</body>
</html>';
$teambody = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Formulaire de contact '.$site_name.'</title>
	</head>
	<body>
		<h1>'.$site_name.' - Ticket '.$data['id'].'</h1>
		<p>'.$nom.' a fermé le ticket '.$data['id'].' de '.$data['expeditor_name'].' ('.htmlspecialchars($data['subject']).')</p>
		<p>Il est possible de réouvrir le ticket dans les 24 heures simplement en y répondant, la discussion sur ce ticket s\'arrêtera ensuite définitivement.</p>
		<a href="'.SITE_URL.'/admin/tickets.php?ticket='.$data['id'].'">Consulter le ticket complet</a></p>
		</body>
		</html>';
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
		$mail->setFrom(SMTP_MAIL, $nom.' via '.$site_name);
		$mail->addReplyTo(SMTP_MAIL, $site_name);
		$mail->addAddress($data['expeditor_email']);
		$mail->Subject = 'Re: "'.htmlspecialchars($data['subject']).'" '.$site_name;
		$mail->CharSet = 'UTF-8';
		$mail->isHTML(TRUE);
		$mail->Body = $body;
		$mail->send();
		$mail2 = new PHPMailer;
		$mail2->isSMTP();
		$mail2->Host = SMTP_HOST;
		$mail2->Port = SMTP_PORT;
		$mail2->SMTPAuth = true;
		$mail2->Username = SMTP_USERNAME;
		$mail2->Password = SMTP_PSW;
		$mail2->setFrom(SMTP_MAIL, $nom.' (admin) via '.$site_name);
		$mail2->addReplyTo(SMTP_MAIL, $site_name);
		$reqt = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
		$reqt->execute();
while($datat = $reqt->fetch()) {
	$req2t = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
	$req2t->execute(array($datat['account_id']));
	while($data2t = $req2t->fetch()) {
		$mail2->addAddress($data2t['email']);
	}
}
	$mail2->Subject = 'Re: ['.$site_name.'] : '.htmlspecialchars($data['subject']);
		$mail2->CharSet = 'UTF-8';
		$mail2->isHTML(TRUE);
		$mail2->Body = $teambody;
		if($mail2->send())
			$log='Ticket fermé, mails envoyés';
	}
	$req = $bdd->prepare('UPDATE `tickets` SET status=4, date=? WHERE `id`=?');
	$req->execute(array(time(), $_GET['close']));
}
if(isset($_GET['delete']) and isset($_POST['del']) and $_POST['del'] == 'SUPPRIMER') {
	$req = $bdd->prepare('DELETE FROM `tickets` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['delete']));
	$log='Ticket supprimé';
}
if(isset($_GET['send']) and isset($_POST['msg'])) {
	$req = $bdd->prepare('SELECT * FROM `tickets` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['send']));
	if($data = $req->fetch()) {
		$msg = str_replace("\n\n", '</p><p>', $_POST['msg']);
		$msg = '<p>'.str_replace("\n", '<br>', $msg).'</p>';
		$msg2= strip_tags(html_entity_decode($msg));
		$msgs = json_decode($data['messages'], true);
		$time = time();
		$msgs[] = ['e'=>$nom, 'm'=>1, 'd'=>$time, 't'=>convertToMD($msg)];
		$larname = $nom.' (Admin)';
		$req2 = $bdd->prepare('UPDATE `tickets` SET `messages`=?, `status`=2, `date`=?, `lastadmreply`=? WHERE `id`=? LIMIT 1');
		$req2->execute(array(json_encode($msgs), $time, $larname, $data['id']));
		$body = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Re: "'.htmlspecialchars($data['subject']).'" '.$site_name.'</title>
		<style type="text/css">#response{border-left:1px solid #0080FF;margin-left:8px;padding: 8px;}</style>
	</head>
	<body>
<p>## Ne pas écrire en-dessous de cette ligne ##</p>
<h1>'.$site_name.'</h1>
		<p>Vous avez reçu une réponse de '.$nom.' pour votre message&nbsp;: <i>'.htmlspecialchars($data['subject']).'</i>.</p>
		<div id="response">'.convertToMD($msg).'</div>
		<hr>
		<p>Pour poursuivre la discussion, utilisez le lien ci-dessous ou répondez simplement à ce message sans en modifier l\'objet.<br>
			<a href="'.SITE_URL.'/contacter.php?reply='.$data['id'].'&h='.$data['hash'].'">'.SITE_URL.'/contacter.php?reply='.$data['id'].'&h='.$data['hash'].'</a></p>
	</body>
</html>';
$teambody = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Re: "'.htmlspecialchars($data['subject']).'" '.$site_name.'</title>
	</head>
	<body>
	<p>## Ne pas écrire en-dessous de cette ligne ##</p>
		<h1>'.$site_name.' - Ticket '.$data['id'].'</h1>
		<p>'.$nom.' a répondu au ticket '.$data['id'].' de '.$data['expeditor_name'].' ('.htmlspecialchars($data['subject']).')</p>
		<p>'.convertToMD($msg).'</p>
		<a href="'.SITE_URL.'/admin/tickets.php?ticket='.$data['id'].'">Consulter le ticket complet</a> ou répondez à ce message sans en modifier l\'objet pour poursuivre la discussion.</p>
		</body>
		</html>';
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
		$mail->setFrom(SMTP_MAIL, $nom.' via '.$site_name);
		$mail->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
		$mail->addAddress($data['expeditor_email']);
		$mail->Subject = 'Re: ['.$site_name.'] : '.htmlspecialchars($data['subject']).' (Ticket #'.$data['id'].'#)';
		$mail->CharSet = 'UTF-8';
		$mail->isHTML(TRUE);
		$mail->Body = $body;
		$mail->send();
		$mail2 = new PHPMailer;
		$mail2->isSMTP();
		$mail2->Host = SMTP_HOST;
		$mail2->Port = SMTP_PORT;
		$mail2->SMTPAuth = true;
		$mail2->Username = SMTP_USERNAME;
		$mail2->Password = SMTP_PSW;
		$mail2->setFrom(SMTP_MAIL, $nom.' (admin) via '.$site_name);
		$mail2->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
		$reqt = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
		$reqt->execute();
while($datat = $reqt->fetch()) {
	$req2t = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
	$req2t->execute(array($datat['account_id']));
	while($data2t = $req2t->fetch()) {
		$mail2->addBCC($data2t['email']);
	}
}
	$mail2->Subject = 'Re: ['.$site_name.'] : '.htmlspecialchars($data['subject']).' (Ticket #'.$data['id'].'#)';
		$mail2->CharSet = 'UTF-8';
		$mail2->isHTML(TRUE);
		$mail2->Body = $teambody;
		if ($mail2->send()) {
			$log='Réponse envoyée';
		}
	}
}
if(isset($_GET['create']) && $_POST['name'] && $_POST['mail'] && $_POST['obj'] && $_POST['msg'])
{
	$obj = $_POST['obj'];
	$msg = str_replace("\n\n", '</p><p>', htmlspecialchars($_POST['msg']));
	$msg = '<p>'.str_replace("\n", '<br>', $msg).'</p>';
	$time = time();
	$req = $bdd->prepare('INSERT INTO `tickets` (`subject`,`expeditor_email`,`expeditor_name`,`messages`,`status`,`hash`,`date`,`lastadmreply`) VALUES (?,?,?,?,0,?,?,?)');
	$message = json_encode([['e'=>$_POST['name'],'m'=>0,'d'=>$time, 't'=>convertToMD($msg)]]);
	$hash = hash('sha512', strval(time()).strval(rand()).$_POST['name'].strval(rand()));
	$req->execute(array($obj, $_POST['mail'], $_POST['name'], $message, $hash, $time, $_POST['name']));
	$req2 = $bdd->prepare('SELECT id FROM tickets WHERE subject=? AND expeditor_email=? AND expeditor_name=? AND messages=? AND hash=? AND date=? AND lastadmreply=?');
	$req2->execute(array($obj, $_POST['mail'], $_POST['name'], $message, $hash, $time, $_POST['name']));
	if($data = $req2->fetch()) {
		$tickid = $data['id'];
	}
		$teambody = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Formulaire de contact '.$site_name.'</title>
	</head>
	<body>
		<h1>'.$site_name.'</h1>
			<p>'.$nom.' a créé un ticket pour '.$_POST['name'].' sur '.$site_name.'&nbsp;:</p>
			<h2>'.htmlspecialchars($_POST['obj']).'</h2>
		<p>'.nl2br(convertToMD($_POST['msg'])).'</p>
			<p><a href="'.SITE_URL.'/admin/tickets.php?ticket='.$tickid.'">Consulter le ticket pour y répondre</a></p>
	</body>
</html>';
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
	$mail->setFrom(SMTP_MAIL, $nom.' via '.$site_name);
	$mail->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
		$req = $bdd2->prepare('SELECT * FROM `team` WHERE `works`="0" OR `works`="2"');
		$req->execute();
while($data = $req->fetch()) {
	$req2 = $bdd2->prepare('SELECT * FROM `accounts` WHERE `id`=?');
	$req2->execute(array($data['account_id']));
	while($data2 = $req2->fetch()) {
		$mail->addBCC($data2['email']);
	}
}
	$mail->Subject = '['.$site_name.'] : '.$_POST['obj'].' (Ticket #'.$tickid.'#)';
		$mail->CharSet = 'UTF-8';
		$mail->isHTML(TRUE);
		$mail->Body = $teambody;
		$mail->send();
		$userbody = '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Formulaire de contact '.$site_name.'</title>
	</head>
	<body>
		<h1>'.$site_name.'</h1>
			<p>'.$nom.' vous a créé un ticket sur '.$site_name.'&nbsp;:</p>
			<h2>'.htmlspecialchars($_POST['obj']).'</h2>
		<p>'.nl2br(convertToMD($_POST['msg'])).'</p><hr>
			<p>Pour nous répondre, utilisez le lien suivante ou répondez à ce message sans en modifier l\'objet&nbsp;:<br>
			<a href="'.SITE_URL.'/contacter.php?reply='.$tickid.'&h='.$hash.'">'.SITE_URL.'/contacter.php?reply='.$ticketid.'&h='.$hash.'</a></p>
	</body>
</html>';
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
	$mail->setFrom(SMTP_MAIL, $nom.' via '.$site_name);
		$mail->addReplyTo(TICKETS_BOT_MAIL, $site_name.' Tickets Bot');
		$mail->addAddress($_POST['mail']);
	$mail->Subject = '['.$site_name.'] : '.$_POST['obj'].' (Ticket #'.$tickid.'#)';
		$mail->CharSet = 'UTF-8';
		$mail->isHTML(TRUE);
		$mail->Body = $userbody;
		if($mail->send())
		{
			$log='Ticket créé, mails envoyés';
		}
}
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Tickets <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
		<link rel="stylesheet" href="/admin/css/tickets.css">
<script type="text/javascript" src="/scripts/default.js"></script>
	</head>
	<body>
<h1>Tickets - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php'; ?>
<ul>
<?php if(isset($_GET['ticket'])) { ?>
<li><a href="tickets.php">Liste des tickets</a></li>
<?php } ?>
</ul>
<?php
if(!empty($log))
	echo '<p role="alert">'.$log.'</p>';
if(isset($_GET['ticket'])) {
	$req = $bdd->prepare('SELECT * FROM `tickets` WHERE `id`=? LIMIT 1');
	$req->execute(array($_GET['ticket']));
	if($data = $req->fetch()) {
		echo '<p>Sujet&nbsp;: <b>'.htmlspecialchars($data['subject']).'</b><br>Expéditeur&nbsp: <b>'.htmlspecialchars($data['expeditor_name']).'</b><!-- (<a href="mailto:'.htmlspecialchars($data['expeditor_email']).'"><b>'.htmlspecialchars($data['expeditor_email']).'</b></a>)--><br>Dernière activité&nbsp;: '.$data['lastadmreply'].' (le '.date('d/m/Y à H:i:s', $data['date']).')<br>Statut&nbsp: <b style="color: #';
		switch($data['status']) {
			case 0: echo 'C00000;">Nouveau'; break;
			case 1: echo '606000;">Non lu'; break;
			case 2: echo '00C000;">En cours'; break;
			case 3: echo '0000C0;">Archivé'; break;
			case 4: echo '000C00;">Fermé'; break;
			default: echo 'black;">Erreur';
		}
		echo '</b></p><table id="ticket_msgs">';
		$messages = json_decode($data['messages'], true);
		foreach($messages as &$msg) {
			echo '<tr class="ticket_msg'.strval($msg['m']).'"><td rowspan="2" class="ticket_msgtd"></td>';
			echo '<td class="ticket_msginfo">';
			if($msg['m'] == 1)
				echo '<img alt="L\'équipe '.$site_name.'" src="/image/logo16.png"> ';
			echo '<b>'.htmlspecialchars($msg['e']).'</b> '.date('d/m/Y H:i:s', $msg['d']).'</td></tr><tr><td>'.$msg['t'].'</td></tr>';
		}
		unset($msg);
		echo '</table>';
		if($data['status'] != 2)
			echo '<p><a href="?waiting='.$data['id'].'">Marquer comme lu</a></p>';
		if($data['status'] != 3)
			echo '<p><a href="?archive='.$data['id'].'">Archiver ce ticket</a></p>';
if($data['status'] == 4 && $data['date']<(time()-86400)) echo 'Ce ticket a été fermé il y a plus de 24 heures, il est impossible d\'y répondre.'; else { ?>
		<form action="?send=<?php echo $data['id']; ?>" method="post">
			<fieldset><legend>Répondre</legend>
				<label for="f1_msg">Message&nbsp;:</label><br>
				<textarea id="f1_msg" name="msg" required rows="20" cols="500"><?php echo "\n\n".$nom.' (Équipe '.$site_name.')'; ?></textarea><br>
				<input type="submit" value="Répondre">
			</fieldset>
		</form>
<?php } ?>
		<form action="?close=<?php echo $data['id']; ?>" method="post" aria-label="Fermer le ticket">
			<fieldset><legend>Fermer</legend>
				<label for="f2_clo">Écrire FERMER en majuscules pour fermer le ticket.</label><br>
				<input type="text" id="f2_clo" name="clo" required><br>
				<input type="submit" value="Fermer">
			</fieldset>
		</form>
		<form action="?delete=<?php echo $data['id']; ?>" method="post" aria-label="Supprimer le ticket">
			<fieldset><legend>Supprimer</legend>
				<label for="f2_del">Écrire SUPPRIMER en majuscules pour supprimer le ticket.</label><br>
				<input type="text" id="f2_del" name="del" required><br>
				<input type="submit" value="Supprimer">
			</fieldset>
		</form>
<?php
	}
	else
		echo '<p>Le ticket n\'existe pas.</p>';
} else {
?>
		<table id="tickets">
			<thead>
				<tr><th>Statut</th><th>Sujet</th><th>Correspondant</th><th>Dernière activité</th></tr>
			</thead>
			<tbody>
<?php
	$req = $bdd->prepare('SELECT * FROM `tickets` ORDER BY `status` ASC, `date` DESC');
	$req->execute();
	$tr2 = false;
	while($data = $req->fetch()) {
		echo '<tr class="ticket';
		if($tr2) echo ' ticket2';
		else echo ' ticket1';
		$tr2 = !$tr2;
		echo '"><td class="ticket_';
		switch($data['status']) {
			case 0: echo '0">Nouveau'; break;
			case 1: echo '1">Non lu'; break;
			case 2: echo '2">En cours'; break;
			case 3: echo '3">Archivé'; break;
			case 4: echo '4">Fermé'; break;
			default: echo '">Erreur';
		}
		echo '</td><td><a href="?ticket='.$data['id'].'">'.htmlspecialchars($data['subject']).'</a></td><td>'.htmlspecialchars($data['expeditor_name']).'</td><td>'.$data['lastadmreply'] .'&nbsp;: le '.date('d/m/Y à H:i:s', $data['date']).'</td></tr>';
	}
?>
			</tbody>
		</table>
<?php } ?>
<details><summary role="heading" aria-level="3">Créer un nouveau ticket</summary>
<p>Remplir le formulaire ci-dessous pour créer un ticket pour un utilisateur</p>
<form action="?create" method="post">
	<fieldset><legend>Informations personnelles</legend>
		<table>
			<tr><td><label for="f_name">Nom de l'utilisateur&nbsp;:</label></td><td><input type="text" name="name" id="f_name" maxlength="255" required></td></tr>
			<tr><td><label for="f_mail">Adresse e-mail de l'utilisateur&nbsp;:</label></td><td><input type="email" name="mail" id="f_mail" maxlength="255" required></td></tr>
		</table>
	</fieldset>
	<fieldset><legend>Message</legend>
		<label for="f_obj">Sujet du message&nbsp;:</label>
		<input type="text" id="f_obj" name="obj" required><br>
		
		<label for="f_msg">Message&nbsp;:</label><br>
		<textarea id="f_msg" name="msg" maxlength="8192" style="width: calc(100% - 10px);min-height: 100px;margin-bottom: 10px;" required></textarea><br>
		<input type="submit" value="Envoyer et créer le ticket" ?>">
	</fieldset>
</form>
</details>
	</body>
</html>