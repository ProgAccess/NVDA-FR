<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/PHPMailer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/Exception.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/phpmailer/src/SMTP.php');
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
if(isset($_GET['form']) and isset($_POST['name']) and isset($_POST['category']) and isset($_POST['keywords']) and isset($_POST['description']) and isset($_POST['text']) and isset($_POST['public'])) {
	$time = time();
	if($_POST['datepub']) $datepubl=$_POST['datepub']; else $datepubl="&&&";
	if($_POST['datedel']) $datesupr=$_POST['datedel']; else $datesupr="&&&";
	$req = $bdd->prepare('INSERT INTO softwares(name,category,text,date,description,keywords,website,author,public,repo,publish_date,deletion_date) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)');
	$req->execute(array(htmlspecialchars($_POST['name']), $_POST['category'], $_POST['text'], $time, $_POST['description'], $_POST['keywords'], $_POST['website'], htmlspecialchars($nom), $_POST['public'], $_POST['repo'], $datepubl, $datesupr));
	$req = $bdd->prepare('SELECT id FROM softwares WHERE name=? AND category=? AND date=? AND description=? AND keywords=?');
	$req->execute(array(htmlspecialchars($_POST['name']), $_POST['category'], $time, $_POST['description'], $_POST['keywords']));
	if($data = $req->fetch()) {
		header('Location: sw_mod.php?addfile='.$data['id']);
if(isset($_POST['publier'])) {
$messagesocial = 'Nouvel article : '.$_POST['name'].' (A'.$data['id'].').'."\n\n".SITE_URL.'/a'.$data['id'].' '.$nom;
			include_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
			send_twitter($messagesocial);
			require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
			send_mastodon($messagesocial);
			switch($_POST['public']) {
		case '0': $statepublic1 = 'Brouillon'; break;
		case '1': $statepublic1 = 'Public'; break;
	}
		}
		if(isset($_POST['mailing'])) {
			switch($_POST['public']) {
		case '0': $statepublic1 = 'Brouillon'; break;
		case '1': $statepublic1 = 'Public'; break;
	}
			$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->Host = SMTP_HOST;
		$mail->Port = SMTP_PORT;
		$mail->SMTPAuth = true;
		$mail->Username = SMTP_USERNAME;
		$mail->Password = SMTP_PSW;
		$mail->setFrom(SMTP_MAIL, $site_name);
		$mail->addReplyTo(ADMIN_ML_ADDR, $site_name.'-Admin');
		$mail->addAddress(ADMIN_ML_ADDR);
		$mail->Subject = '[INFO AUTO] : Nouvel article sur '.$site_name;
		$mail->CharSet = 'UTF-8';
		$mail->isHTML(TRUE);
		$body = '<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8"><title>[INFO AUTO] : Nouvel article sur '.$site_name.'</title></head><body><h1>Nouvel article</h1><p>Un nouvel article a été publié par '.$nom.' sur '.$site_name.' :</p><ul><li>Titre : '.$_POST['name'].'</li><li>État : '.$statepublic1.'</li>';if($_POST['public'] == 0 && $_POST['datepub']) { $body.='<li>Date de publication&nbsp;: '.strftime('%A %d/%m/%Y', strtotime($_POST['datepub'])).'</li>'; } if($_POST['datedel']) { $body.='<li>Date de suppression&nbsp;: '.strftime('%A %d/%m/%Y', strtotime($_POST['datedel'])).'</li>'; } $body.='<li><a href="'.SITE_URL.'/a'.$data['id'].'">Consulter l\'article</a></li></ul></body></html>';
		$mail->Body = $body;
		$mail->send();
		}
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
	}
	$req->closeCursor();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Ajout d'un logiciel sur <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
<script type="text/javascript" src="/scripts/default.js"></script>
	</head>
	<body>
<h1>Ajout logiciel - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php'; ?>
		<form action="?form" method="post">
			<label for="f_name">Nom&nbsp;:</label><input type="text" name="name" id="f_name" maxlength="255" required autofocus><br>
			<label for="f_category">Catégorie&nbsp;:</label><select name="category" id="f_category" onchange="showother()"><?php
$req = $bdd->query('SELECT * FROM softwares_categories ORDER BY name ASC');
while($data = $req->fetch()) {echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';}
$req->closeCursor()
?></select><br>
			<label for="f_keywords">Mots clés&nbsp;:</label><input type="text" name="keywords" id="f_keywords" maxlength="255" required><br>
			<label for="f_description">Description courte&nbsp;:</label><input type="text" name="description" id="f_description" maxlength="1024" required><br>
			<label for="f_website" id="f_website02">Type d'extension&nbsp;:</label><select id="f_website" name="website">
			<option value="Applicative">Applicative</option>
			<option value="Globale">Globale</option>
			<option value="Mixt">Mixt</option>
			</select><br>
			<label for="f_repo" id="f_repo02">Adresse du site ou du dépôt&nbsp;:</label><input type="text" name="repo" id="f_repo" maxlength="255"><br>
			<label for="f_text">Texte long (HTML ou MarkDown)&nbsp;:</label><br>
			<textarea name="text" id="f_text" maxlength="20000" rows="20" cols="500" required></textarea><br>
			<p>Il est possible de modifier ces informations et de rajouter des liens et fichiers ultérieurement.</p>
			<label for="ppp556">État de l'article&nbsp;:</label>
			<select id="ppp556" name="public" onchange="showother()">
			<option value="0">Brouillon (accessible uniquement via l'interface d'admin)</option>
			<option value="1" selected>Public (affiché dans la liste des articles)</option>
			</select><br>
			<label for="ppp555" id="ppp555_2">Annoncer l'ajout sur les réseaux sociaux&nbsp;:</label>
			<input type="checkbox" id="ppp555" name="publier" checked><br>
			<label for="ppp557" id="ppp557_2">Annoncer l'ajout par mail sur NVDA-FR-Admin&nbsp;:</label>
			<input type="checkbox" id="ppp557" name="mailing" checked><br>
			<label for="l_datepub" id="l_datepub_2">Date de publication&nbsp;:</label>
			<input type="date" id="l_datepub" name="datepub" min="<?php echo date('Y-m-d', time()); ?>"><br>
			<script type="text/javascript">
function showother() {
	if(document.getElementById("f_category").value == "1") {
		document.getElementById("f_website02").style = "";
		document.getElementById("f_website").style = "";
		document.getElementById("f_repo02").style = "";
		document.getElementById("f_repo").style = "";
	} else {
		document.getElementById("f_website02").style = "display: none;";
		document.getElementById("f_website").style = "display: none;";
		document.getElementById("f_repo02").style = "display: none;";
		document.getElementById("f_repo").style = "display: none;";
	}
	if(document.getElementById("ppp556").value == "0") {
		document.getElementById("ppp555_2").style = "display: none;";
		document.getElementById("ppp555").style = "display: none;";
		document.getElementById("ppp557_2").style = "";
		document.getElementById("ppp557").style = "";
		document.getElementById("l_datepub_2").style = "";
		document.getElementById("l_datepub").style = "";
		document.getElementById("ppp555").checked = false;
		document.getElementById("ppp557").checked = true;
	} else {
		document.getElementById("ppp555_2").style = "";
		document.getElementById("ppp555").style = "";
		document.getElementById("ppp557_2").style = "";
		document.getElementById("ppp557").style = "";
		document.getElementById("l_datepub_2").style = "display: none;";
		document.getElementById("l_datepub").style = "display: none;";
		document.getElementById("ppp555").checked = true;
		document.getElementById("ppp557").checked = true;
	}
}
showother();
</script>
			<label for="l_datedel">Date de suppression&nbsp;:</label>
			<input type="date" id="l_datedel" name="datedel" min="<?php echo date('Y-m-d', time()); ?>"><br>
			<input type="submit" value="Ajouter">
		</form>
	</body>
</html>