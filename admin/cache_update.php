<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
if(isset($_GET['cache'])) {
	$cachedir = $_SERVER['DOCUMENT_ROOT'].'/cache/';
	if($_GET['cache'] == 'all' or $_GET['cache'] == 'journal') {
		include($_SERVER['DOCUMENT_ROOT'].'/tasks/history_cache.php');
	header('Location: /admin/');
	}
	if($_GET['cache'] == 'all' or $_GET['cache'] == 'metadata') {
		$req = $bdd->prepare('
		SELECT `softwares_files`.`id`, `softwares_files`.`hash`, `softwares_files`.`sw_id`, `softwares`.`category`
		FROM `softwares`
		LEFT JOIN `softwares_files` ON `softwares`.`id`=`softwares_files`.`sw_id`');
		$req->execute();
		while($data=$req->fetch()) {
			if(in_array($data['category'], CAT_MOREINFO)) {
				require_once($_SERVER['DOCUMENT_ROOT'].'/include/addon_info.php');
				$metadata = json_encode(addon_info($_SERVER['DOCUMENT_ROOT'].'/files/'.$data['hash']));
				$req2 = $bdd->prepare('UPDATE `softwares_files` SET `metadata`=? WHERE `id`=? LIMIT 1');
				$req2->execute(array($metadata, $data['id']));
			}
		}
	header('Location: /admin/');
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Mise à jour des caches de <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
<script type="text/javascript" src="/scripts/default.js"></script>
	</head>
	<body>
<h1>Caches - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php'; ?>
		<p><a href="?cache=all">Mettre à jour tous les caches</a></p>
		
<ul>
<li><a href="?cache=journal">Mettre à jour le cache du journal des modifications</a></li>
<li><a href="?cache=metadata">Mettre à jour les metadata des extensions</a></li>
</ul>
	</body>
</html>