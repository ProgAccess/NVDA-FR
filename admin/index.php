<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Administration - <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
<script src="/scripts/default.js"></script>
</head>
<body>
<h1>Administration - <a href="/"><?php print $site_name; ?></a></h1>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php'; ?>
<h2>Connecté en tant que&nbsp;: <?php print $nom; ?></h2>
<ul>
<li><a href="sw_mod.php">Modifier un article</a></li>
<li><a href="tickets.php">Consulter les tickets</a></li>
<li><a href="UpdateDoc.php">Mettre à jour les fichiers de documentation</a></li>
<li><a href="publication.php">Publier un message social</a></li>
<li><a href="sw_categories.php">Gérer les catégories</a></li>
<li><a href="cache_update.php">Gestionnaire caches</a></li>
<li><a href="adminer.php">Bases de données</a></li>
</ul>
<h2>Liste des extensions sans adresse de dépôt</h2>
<?php
$req=$bdd->Query('SELECT * FROM `softwares` WHERE `category`=1');
echo '<ul>';
$nbsans=0;
$nbavec=0;
while($data=$req->fetch())
{
	if($data['id'] != '50')
	{
		if($data['repo'] == '')
		{
			echo '<li>'.$data['name'].'</li>';
			$nbsans++;
		}
		$nbavec++;
	}
}
echo '<li>'.$nbavec.' extensions recensées dont '.$nbsans.' sans adresse renseignée</li></ul>';
?>
</body>
</html>