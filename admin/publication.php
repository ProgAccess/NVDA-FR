<?php
$logonly = true;
$adminonly = true;
$justnvda = true;
include($_SERVER['DOCUMENT_ROOT'].'/include/log.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/include/consts.php';
$log = '';
if(isset($_GET['form']) and isset($_POST['platform']) and isset($_POST['msg'])) {
	if($_POST['platform'] == 'fb' or $_POST['platform'] == 'all') {
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/facebook/envoyer.php');
		send_facebook($_POST['msg']);
		$log .= 'Publication postée !';
	}
	if($_POST['platform'] == 'tw' or $_POST['platform'] == 'all') {
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/twitter/twitter.php');
		send_twitter($_POST['msg']);
		$log .= 'Tweet posté !';
	}
	if($_POST['platform'] == 'masto' or $_POST['platform'] == 'all') {
		require_once($_SERVER['DOCUMENT_ROOT'].'/include/lib/Mastodon/mastodon_publisher.php');
		send_mastodon($_POST['msg']);
		$log .= 'Pouet posté !';
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Publication sur les réseaux - <?php print $site_name; ?></title>
<?php print $cssadmin; ?>
		<script type="text/javascript" src="/scripts/default.js"></script>
	</head>
	<body>
		<h1>Messages sociaux - <a href="/"><?php print $site_name; ?></a></h1>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/include/loginbox.php';
if(!empty($log)) print '<p role="alert"><b>'.$log.'</b></p>'; ?>
		<form action="?form" method="post">
			<label for="f_platform">Publier sur&nbsp;:</label>
			<select id="f_platform" name="platform">
				<option value="all" selected>Tous les réseaux</option>
				<!--<option value="fb">Facebook</option> -->
				<option value="masto">Mastodon</option>
				<option value="tw">Twitter</option>
			</select><br>
			<label for="f_msg">Message à poster&nbsp;:</label>
			<textarea id="f_msg" name="msg" autocomplete="off" maxlength="280" rows="20" cols="100" required></textarea>
<input type="submit" value="Publier">
		</form>
	</body>
</html>