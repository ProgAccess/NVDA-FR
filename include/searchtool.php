<div id="searchtool" role="search">
	<form action="/search.php" method="get" aria-label="Rechercher">
	<label for="searchtool_text" style="position:absolute; top:-999px; left:-9999px;">Rechercher</label>
		<input id="searchtool_text" type="search" name="q" placeholder="Rechercher" aria-label="Rechercher"><br>
		<select id="searchtool_cat" title="Catégorie" name="c[]" aria-label="Catégorie" multiple="multiple" size="1"><option value="" selected>Tout</option><option value="2">Téléchargement</option><option value="5">Actualités</option><option value="1">Extensions</option><option value="4">Braille</option><option value="3">Voix</option></select>
		<input id="searchtool_go" type="submit" value="Rechercher">
	</form>
</div>