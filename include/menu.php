<nav id="nav">
<ul style="position:absolute; top:-999px; left:-9999px;">
<li><a href="#contenu" accesskey="c">Contenu</a></li>
<li><a href="#footer" accesskey="b">Bas de page</a></li>
</ul>
<ul role="navbar">
<li><a href="/" <?php if($_SERVER['DOCUMENT_URI'] == '/index.php') echo 'aria-current="page"'; ?>>Accueil</a></li>
<li><a href="/doc.php" <?php if($_SERVER['DOCUMENT_URI'] == '/doc.php') echo 'aria-current="page"'; ?>>Documentation</a></li>
<li><a href="/c2" <?php if($_SERVER['DOCUMENT_URI'] == '/cat.php' && $_REQUEST['id'] == 2) echo 'aria-current="page"'; ?>>Téléchargements</a></li>
<li><a href="/c5" <?php if($_SERVER['DOCUMENT_URI'] == '/cat.php' && $_REQUEST['id'] == 5) echo 'aria-current="page"'; ?>>Actualités</a></li>
<li><a href="/a50" <?php if($_SERVER['DOCUMENT_URI'] == '/article.php' && $_REQUEST['id'] == 50) echo 'aria-current="page"'; ?>>Extensions</a></li>
<li><a href="/c4" <?php if($_SERVER['DOCUMENT_URI'] == '/cat.php' && $_REQUEST['id'] == 4) echo 'aria-current="page"'; ?>>Braille</a></li>
<li><a href="/c3" <?php if($_SERVER['DOCUMENT_URI'] == '/cat.php' && $_REQUEST['id'] == 3) echo 'aria-current="page"'; ?>>Voix</a></li>
<li><a href="https://accessikey.nvda-fr.org/">L'AccessiKey</a></li>
<li><a href="/formations.php" <?php if($_SERVER['DOCUMENT_URI'] == '/formations.php') echo 'aria-current="page"'; ?>>Centre de formations à NVDA</a></li>
<li><a href="/inf.php" <?php if($_SERVER['DOCUMENT_URI'] == '/inf.php') echo 'aria-current="page"'; ?>>Contacts</a></li>
<li><a type="application/rss+xml" href="/journal_modif.xml">Flux RSS</a></li>
<li><a href="/donate.php" <?php if($_SERVER['DOCUMENT_URI'] == '/donate.php') echo 'aria-current="page"'; ?>>Faire un don à NV Access</a></li>
</ul>
</nav>