<?php
require_once('config.local.php');

date_default_timezone_set('Europe/Paris'); 
setlocale(LC_TIME,'fr_FR.UTF8');

function urlsafe_b64encode($str) {
	return strtr(preg_replace('/[\=]+\z/', '', base64_encode($str)), '+/=', '-_');
}

function urlsafe_b64decode($data) {
	$data = preg_replace('/[\t-\x0d\s]/', '', strtr($data, '-_', '+/'));
	$mod4 = strlen($data) % 4;
	if($mod4)
		$data .= substr('====', $mod4);
	return base64_decode($data);
}

function getLastGitCommit()
{
	global $site_name;
	$hash = shell_exec('git --git-dir="'.GIT_DIR.'" rev-parse --verify HEAD');
	$commitDate = strftime('%d/%m/%Y à %H:%M', shell_exec('git --git-dir="'.GIT_DIR.'" show -s --format=%ct '.$hash));
	$commitURL = '<a href="'.GIT_COMMIT_BASE_URL.$hash.'">Commit '.rtrim(shell_exec('git --git-dir="'.GIT_DIR.'" show -s --format=%h')).'</a>';
echo $site_name.' - Version du '.$commitDate.' ('.$commitURL.')';
}

function isDev()
{
if(strstr($_SERVER['HTTP_HOST'], 'dev.') || DEV == true)
return true;
else
return false;
}

$site_name = (isDev()?SITE_NAME.'-Dev':SITE_NAME);
$cssadmin='<link rel="stylesheet" href="/admin/css/admin.css">';
require_once('dbconnect.php');

$css_path = '';
?>