<head>
<title><?php echo $titre.' – '.$site_name; ?></title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="/css/reset.css">
<link rel="stylesheet" type="text/css" href="/css/nvda_fr.css">
<?php echo $css_path; ?>
<script src="/scripts/default.js"></script>
</head>