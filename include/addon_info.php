<?php
/**
 * Fonction addon_info
 * un paramètre : le fichier du module complémentaire NVDA
 * Retourne un tableau avec les clé suivantes :
 * author : l'auteur du module complémentaire
 * version : sa version
 * title : le titre du module (summary dans manifest.ini)
 * description : sa description, même nom dans manifest.ini
 * NVDAMin : version minimale de NVDA supportée (minimumNVDAVersion dans manifest.ini)
 * NVDAMax : dernière version testée de NVDA (lastTestedNVDAVersion dans manifest.ini)
 * Si erreur, retourne null
*/
function addon_info($fichier) {
	$zip = zip_open($fichier);
	if ($zip) {
		while ($zip_entry = zip_read($zip)) {
			switch (zip_entry_name($zip_entry)) {
				case "manifest.ini":
					if (zip_entry_open($zip, $zip_entry, "r")) {
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						$manifest_name=date("YmdHis").mt_rand().".ini";
						$manifest = fopen($manifest_name, "w");
						if ($manifest) {
							fwrite($manifest, $buf);
							fclose($manifest);
							$manifest_data = parse_ini_file($manifest_name);
							unlink($manifest_name);
						} else return;
						zip_entry_close($zip_entry);
					} else return;
					break;
				case "locale/fr/manifest.ini":
					if (zip_entry_open($zip, $zip_entry, "r")) {
						$buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						$manifest_name=date("YmdHis").mt_rand().".ini";
						$manifest = fopen($manifest_name, "w");
						if ($manifest) {
							fwrite($manifest, $buf);
							fclose($manifest);
							$manifest_fr_data = parse_ini_file($manifest_name);
							unlink($manifest_name);
						} else return;
						zip_entry_close($zip_entry);
					} else return;
					break;
				default:
					break;
			}
			if (isset($manifest_data) && is_array($manifest_data) && isset($manifest_fr_data) && is_array($manifest_fr_data))
				break;
		}
		zip_close($zip);
		if (isset($manifest_data) && is_array($manifest_data) && !empty($manifest_data)) {
			$addon_info = array();
			$addon_info['author'] = $manifest_data['author'];
			$addon_info['version'] = $manifest_data['version'];
			$addon_info['NVDAMin'] = $manifest_data['minimumNVDAVersion'];
			$addon_info['NVDAMax'] = $manifest_data['lastTestedNVDAVersion'];
			if (isset($manifest_fr_data) && is_array($manifest_fr_data) && !empty($manifest_fr_data)) {
				$addon_info['title'] = $manifest_fr_data['summary'];
				$addon_info['description'] = $manifest_fr_data['description'];
			} else {
				$addon_info['title'] = $manifest_data['summary'];
				$addon_info['description'] = $manifest_data['description'];
			}
		} else return;
		return $addon_info;
	} else return;
}

/*
 * $addon = addon_info("./addon.nvda-addon");
 * print_r($addon);
*/
?>
