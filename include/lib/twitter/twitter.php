<?php
$document_root = __DIR__.'/../../..';
require_once $document_root.'/include/config.local.php';
require_once $document_root.'/include/lib/ca-bundle/src/CaBundle.php';
require_once $document_root.'/include/lib/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;
function send_twitter($message) {
	$connection = new \Abraham\TwitterOAuth\TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
	$post_tweets = $connection->post("statuses/update", ["status" => $message]);
}

function getTweets($count)
{
	$connection = new \Abraham\TwitterOAuth\TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
	$tweets = $connection->get('statuses/user_timeline', ['count' => $count, 'exclude_replies' => true]);
	return $tweets;
}

function formatTweetText($text)
{
    $text = preg_replace('/(https?:\/\/\S+)/', '<a href="$1" target="_blank">$1</a>', $text);
    $text = preg_replace('/@([A-Za-z0-9_]{1,15})/', '<a href="https://twitter.com/$1" target="_blank">@$1</a>', $text);
    $text = preg_replace('/#([A-Za-z0-9_]+)/', '<a href="https://twitter.com/hashtag/$1" target="_blank">#$1</a>', $text);
    return $text;
}
?>