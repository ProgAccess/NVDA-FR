<?php
function urank($rank, $user='', $er=true) {
	global $tr0;
	$s = '';
	if($user == '') {
		switch($rank) {
			case '0': return '<span class="rk rk1 rk_0">Nouveau</span>';break;
			case '1': return '<span class="rk rk1 rk_1">Membre</span>';break;
			case 'a': return '<span class="rk rk1 rk_a">Membre de l\'équipe</span>';break;
			case 'm': return '<span class="rk rk1 rk_m">Modérateur</span>';break;
			case 'i': return '<span class="rk rk1 rk_i">Anonyme</span>';break;
			case 'b': return '<span class="rk rk1 rk_b">Banni</span>';break;
		}
	} else {
		switch($rank) {
			case '0': if($er)$s='<span class="rk2r">&#x20;(Nouveau)</span>';return '<span class="rk rk2 rk_0" title="Nouveau">'.$user.'</span>'.$s;break;
			case '1': if($er)$s='<span class="rk2r">&#x20;(Membre)</span>';return '<span class="rk rk2 rk_1" title="Membre">'.$user.'</span>'.$s;break;
			case 'a': if($er)$s='<span class="rk2r">&#x20;(Membre de l\'équipe)</span>';return '<span class="rk rk2 rk_a" title="Membre de l\'équipe">'.$user.'</span>'.$s;break;
			case 'm': if($er)$s='<span class="rk2r">&#x20;(Modérateur)</span>';return '<span class="rk rk2 rk_m" title="Modérateur">'.$user.'</span>'.$s;break;
			case 'i': if($er)$s='<span class="rk2r">&#x20;(Anonyme)</span>';return '<span class="rk rk2 rk_i" title="Anonyme">'.$user.'</span>'.$s;break;
			case 'b': if($er)$s='<span class="rk2r">&#x20;(Banni)</span>';return '<span class="rk rk2 rk_b" title="Banni">'.$user.'</span>'.$s;break;
		}
	}
	return '';
}
?>