<?php
$permalink = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$titremodifie = str_replace(' ',' ',$titre);
?>
<div id="footer" role="contentinfo">
<div id="social_share" role="complementary">
<p>Partager cette page sur&nbsp;:</p>
<ul>
<li><a href="https://www.facebook.com/sharer.php?u=<?php print $permalink; ?>&t=<?php print $titremodifie; ?>" target="_blank" title="Facebook"><img src="/images/facebook.png" alt="Facebook"></a></li>
<li><a href="https://twitter.com/share?url=<?php print $permalink; ?>&text=<?php print $titremodifie; ?>&via=<?php echo $site_name; ?>" target="_blank" title="Twitter"><img src="/images/twitter.png" alt="Twitter"></a></li>
</ul>
</div>
<a href="/inf.php">Contactez-nous</a><br>
<a href="<?php echo TWITTER_URL; ?>" target="_blank" title="<?php echo $site_name; ?> sur Twitter"><img src="/images/twitter.png" alt="<?php echo $site_name; ?> sur Twitter"></a><br>
<a rel="me" href="<?php echo MASTO_URL; ?>" target="_blank" title="<?php echo $site_name; ?> sur Mastodon"><img src="/images/mastodon-purple.svg" alt="<?php echo $site_name; ?> sur Mastodon"></a><br>
<?php include('stats.php'); ?>
<p>Ce site n'est pas affilié à <a href="https://www.nvaccess.org/">NVAccess (développeur de NVDA)</a>.<br>
L'équipe de ce site ne comporte aucun développeur de NVDA, pour les suggestions de nouvelles fonctionnalités, veuillez consulter <a href="https://github.com/nvaccess/nvda">le GitHub de NVAccess</a>.</p>
<p>Le <a href="https://github.com/nvaccess/nvda">code source de NVDA</a> est disponible sur GitHub.<br>
Le <a href="https://gitlab.com/ProgAccess/NVDA-FR">code source de <?php echo $site_name; ?></a> est disponible sur GitLab.</p>
<p>Copyright &copy; 2006-<?php print date('Y').' '.$site_name; ?> et <a href="https://www.progaccess.net">ProgAccess</a></p>
<div role="navigation">
<ul style="position:absolute; top:-999px; left:-9999px;">
<li><a href="#header" accesskey="h">Haut de page</a></li>
</ul>
<?php getLastGitCommit(); ?>
</div>
</div>