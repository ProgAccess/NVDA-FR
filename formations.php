<?php set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$stats_page = 'formations';
$titre="Formations"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<p>Nous vous proposons ici de retrouver toute une liste d'associations ou de particuliers qui pourront vous former à l'utilisation du lecteur d'écran NVDA.<br>
Cette page est organisée en fonction des villes de chaque formateur indiqué.<br>
Si vous proposez vous même une formation à NVDA et que vous voudriez apparaître sur cette page : <a href="/inf.php">contactez-nous</a>.</p>
<h3>Paris et ses environs</h3>
<h4>Association AIR</h4>
<p>Cette association propose des formations à l'informatique adapté en général, sur PC ou Mac, ou bien sur smartphone Android ou IOS.<br>
Dans le cadre de NVDA, la formation sur PC sera bien entendu la plus appropriée.</p>
<ul>
<li>Adresse : 4 Rue Auber, 75009, Paris</li>
<li>Tél : 01 40 06 00 60</li>
<li><a href="http://www.air-asso.org/">Site Web</a></li>
</ul>
<h3>Les AVH locales</h3>
<p>La liste ci-dessous vous indique le nom de la ville et les coordonnées à notre disposition :</p>
<ul>
<li>Évreux : 02 32 38 05 66</li>
<li>Isle : 05 55 50 40 19 (06 81 23 12 39)</li>
<li>Rouen : 02 35 71 18 55</li>
<li>Versailles : 01 39 50 75 20</li>
<li>Limoges : 05 55 77 53 03 (06 81 23 12 39)</li>
<li>Le Mans : 02 43 24 20 13</li>
<li>Paris Ile de France : 01 40 44 67 25</li>
<li>Tarbes (Hautes-Pyrénées) : 05 81 59 90 31 (21 Rue du IV Septembre, 65000 Tarbes)</li>
<li>Reims (Marne) : 03 26 85 27 11 (109 Rue du Barbâtre, 51100 Reims)</li>
<li><a href="http://annecy.avh.asso.fr/">Annecy</a>&nbsp;: 04 50 45 37 27 - <a href="mailto:comite.annecy@avh.asso.fr">comite.annecy@avh.asso.fr</a></li>
</ul>
<h3>Autres</h3>
<h4>UnivInfo</h4>
<ul>
<li>Formateur&nbsp;: Johan Senn</li>
<li>Tél&nbsp;: 06 71 23 07 84</li>
<li>Mail&nbsp;: <a href="mailto:contact@univinfo.fr">contact@univinfo.fr</a></li>
<li>Organisation&nbsp;: en distanciel</li>
</ul>
<h4>M. Thibaud NÉDEY</h4>
<p>M. Thibaud NÉDEY se propose de former à l'informatique adapté en général, quelque soit votre niveau d'origine.<br>
Ce particulier se déplace à votre domicile ou bien sur votre lieu de travail dans toute la Franche-Comté.<br>
Ses formations peuvent être financées par les services tels que les MDPH ou les AGEFIP.</p>
<ul>
<li>Tél : 06 75 59 16 73</li>
<li>Mail : <a href="mailto:contact@tybot.fr">contact@tybot.fr</a></li>
<li>Site Web : <a href="http://www.tybot.fr/">http://www.tybot.fr/</a></li>
</ul>
<h4>Yannick Plassiard</h4>
<p>"Les formations proposées incluent :</p>
<ul>
<li>Formation de base à NVDA pour les particuliers et professionnels</li>
<li>Etude de faisabilité et audit d’adaptation de poste informatique avec NVDA</li>
<li>Création de modules complémentaires personnalisés pour améliorer ou rendre accessible une application qui ne le serait pas déjà</li>
</ul>
<p>Autres formations que je peux dispenser :</p>
<ul>
<li>Initiation à la Musique assistée par Ordinateur avec Reaper et NVDA ou VoiceOver</li>
<li>Initiation à la programmation en Python ou d’autres langages</li>
</ul>
<p>Ces formations peuvent être faites :</p>
<ul>
<li>Sur site</li>
<li>À distance via un logiciel permettant une interaction optimale</li>
</ul>
<p>Contact Mail : <a href="mailto:plassiardyannick@gmail.com">plassiardyannick@gmail.com</a>"</p>
<h4>FR-ATF</h4>
<p>Nouvel organisme basé dans le Grand Est</p>
<ul>
<li>Tél&nbsp;: 07 61 83 81 57</li>
<li>Mail&nbsp;: <a href="mailto:contact@fr-atf.com">contact@fr-atf.com</a></li>
<li>Site&nbsp;: <a href="http://www.fr-atf.com">http://www.fr-atf.com</a></li>
</ul>
<h4>Pierre-Emmanuel RAMÉ</h4>
<p>Les prestations proposées vont de l'apprentissage de base jusqu'aux audits et adaptations de poste de travail avec le développement d'adaptations afin de rendre un site Internet ou une application métier plus simple et plus rapide à utiliser si nécessaire.<br>
Les formations peuvent s'effectuer sur site ou à distance via l'extension NVDA Remote.<br>
Pour les particuliers les formations peuvent être prise en charge par la MDPH, et pour les professionnels, elles peuvent être financer par l'AGEFIPH ou le FIPHFP.</p>
<ul>
<li>Tél&nbsp;: 06 71 11 37 76</li>
<li>Mail&nbsp;: <a href="mailto:pierre-emmanuel@pe-rame.fr">pierre-emmanuel@pe-rame.fr</a></li>
</ul>
<p>Si vous constatez une information qui n'est plus valable sur cette page, merci de nous le faire savoir immédiatement, dans le but notamment de ne pas importuner les personnes qui pourraient avoir récupérés le numéro de téléphone d'un ancien formateur. Merci !</p>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>