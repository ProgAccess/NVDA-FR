﻿<?php set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$stats_page = 'doc';
$titre="Documentation"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<h2>Documentation utilisateur</h2>
<ul>
<li><a href="/doc/userGuide.html">Le guide de l'utilisateur NVDA</a> contient toutes les informations utiles à la mise en œuvre et à l'utilisation de NVDA</li>
<li><a href="/doc/changes.html">L'historique des changements de NVDA</a> contient l'ensemble des changements intervenus depuis la première version de NVDA</li>
<li><a href="/doc/keyCommands.html">Le Résumé des commandes de NVDA</a> donne un résumé des principales commandes de NVDA</li>
</ul>
<h2>Documentation technique</h2>
<p>Note&nbsp;: Les documents suivants sont en Anglais.</p>
<ul>
<li><a href="https://www.nvaccess.org/files/nvda/documentation/developerGuide.html">Le Developer Guide</a> contient les informations permettant à un programmeur d'écrire ses propres modules complémentaires (équivalents au scripts qu'on trouve dans d'autres revues d'écran)</li>
</ul>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>