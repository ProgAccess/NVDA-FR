<?php
set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$titre='Résultats de recherche';
$stats_page='recherche';
$css_path .= '<link rel="stylesheet" href="/css/search.css">';
$searchterms = '';
if(isset($_GET['q']) and $_GET['q'] != '' and strlen($_GET['q']) <= 255) {
	$searchterms = $_GET['q'];
	$args['q'] = $searchterms;
	if($searchterms == "﷐") {
		header('Location: https://fr.wikipedia.org/wiki/Basilic_(mythologie)');
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
<?php require_once('include/header.php'); ?>
<body>
<div id="container">
<?php include('include/banner.php');
include('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php
if(!empty($searchterms))
	echo 'Rechercher '.htmlspecialchars($_GET['q']);
?></h1>
<?php
if(!empty($searchterms)) {
	$req = $bdd->query('SELECT * FROM `softwares_categories`');
	$cats = array();
	while($data = $req->fetch()) {$cats[$data['id']] = $data['name'];}
	
	$atime = microtime();
	$terms = explode(' ', $searchterms);
	$results = array();
	$where = '';
	$cat = array();
	if(isset($_GET['c']) and !empty($_GET['c']) and count($_GET['c']) <= count($cats)) {
		foreach($_GET['c'] as &$val) {
			if(!empty($val))
				$cat[] = $val;
		}
		
		if(!empty($cat))
			$where = ' WHERE `category`=?';
		if(count($cat) > 1)
			$where .= str_repeat(' OR `category`=?', count($cat)-1);
	}
	
	$entries = [];
	$req = $bdd->prepare('SELECT * FROM `softwares` '.$where);
	$req->execute($cat);
	while($data = $req->fetch()) {
		if(!isset($entries[$data['id']]))
			$entries[$data['id']] = array('cat'=>$data['category'], 'hits'=>$data['hits'], 'dl'=>$data['downloads'], 'id'=>$data['id'], 'title'=>$data['name'], 'tags'=>$data['keywords'], 'desc'=>$data['description']);
	}
	foreach($entries as $id => $entry) {
		$tags = explode(' ', $entry['tags']);
		$pts = intval($terms[0] == '*');
		if($pts) array_shift($tags);
		foreach($terms as &$term) {
			$imp = 3;
			foreach($tags as &$tag) {
				if($term == $tag)
					$pts += 12+$imp**2;
				else {
					$lev = levenshtein($term, $tag);
					if($lev <= 2)
						$pts += 5-$lev+$imp;
					$lev = levenshtein(metaphone($term), metaphone($tag));
					if($lev < 2)
						$pts += 5-$lev+$imp;
				}
			}
			if($imp > 0) $imp --;
			unset($tag);
		}
		unset($term);
		if($pts > 0)
			$results[] = array('id'=>$id, 'title'=>$entry['title'], 'cat'=>$entry['cat'], 'desc'=>$entry['desc'], 'hits'=>$entry['hits'], 'dl'=>$entry['dl'], 'pts'=>$pts);
	}
	// remove the first occurence of v in a
	function array_remove($a, $v) {
		$r = array();
		$o = false;
		foreach($a as &$k) {
			if($k != $v or $o)
				$r[] = $k;
			else
				$o = true;
		}
		unset($k);
		return $r;
	}
	$btime = microtime() - $atime;
	if(count($results) == 0)
		echo '<span id="log">Désolé, nous n\'avons rien trouvé pour <span class="log_quote">'.htmlentities($searchterms).'</span></span>';
	else
		echo '<p id="timelog">'.count($results).' résultats trouvés en '.intval($btime*1000000/1000).' ms</p>';
	while(count($results) > 0) {
		$max = array('pts'=>0);
		foreach($results as &$rs) {
			if($rs['pts'] > $max['pts'])
				$max = $rs;
		}
		unset($rs);
		$results = array_remove($results, $max);
		echo '<div class="result"><a href="/a'.$max['id'].'"><h2 class="rs_title">'.$max['title'].'</h2></a><span class="rs_cat">('.$cats[$max['cat']].')</span>'. (isDev()?'<span class="rs_pts">'.$max['pts'].'</span>':'') .'<p class="rs_text">'.$max['desc'].'</p><span class="rs_meta">';
		echo $max['hits'].' visites, '.$max['dl'].' téléchargements</span></div>';
	}
}
?>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>