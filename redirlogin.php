<?php $logonly = true;
set_include_path($_SERVER['DOCUMENT_ROOT']);
require 'include/log.php';
require_once 'include/consts.php';
$titre="Connexion réussie !";
$stats_page = 'redirlogin'; ?>
<!doctype html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php include('include/banner.php');
include('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<p>Bonjour <?php echo $login['username']; ?>, votre identification est réussie !<br>
Que souhaitez-vous faire ensuite ?</p>
<ul>
<?php if(isset($login['rank']) && $login['rank'] == 'a') {
$req = $bdd2->prepare('SELECT `works` FROM `team` WHERE `account_id`=? LIMIT 1');
				$req->execute(array($login['id']));
				if($data = $req->fetch()) {
					$worksnum = $data['works'];
				}
				if($worksnum == '0' or $worksnum == '2') { ?>
<li><a href="/admin">Aller à l'administration (<?php echo $site_name; ?>)</a></li>
				<?php }
				if($worksnum == '1' or $worksnum == '2') { ?>
					<li><a href="https://www.progaccess.net/admin?cid=<?php print $_COOKIE['connectid']; ?>&ses=<?php print $_COOKIE['session']; ?>">Aller à l'administration (ProgAccess)</a></li>
				<?php } } ?>
<li><a href="/">Aller à l'accueil</a></li>
<li><a href="https://www.progaccess.net/home.php">Consulter mon profil (Redirection vers ProgAccess)</a></li>
<li><a href="/logout.php?token=<?php echo $login['token']; ?>">Se déconnecter</a></li>
</ul>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>