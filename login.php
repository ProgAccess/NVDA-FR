<?php
$nolog = true;
require 'include/log.php';
$stats_page='login';
set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once 'include/consts.php';
$titre="S'identifier";

$log = '';
if(isset($_POST['username']) and isset($_POST['psw'])) {
	require_once('include/lib/random/random.php');
	
	$req = $bdd2->prepare('SELECT * FROM `accounts` WHERE `username`=? LIMIT 1');
	$req->execute(array($_POST['username']));
	
	if($data = $req->fetch()) {
		if(password_verify($_POST['psw'], $data['password'])) {
			$session = hash('sha512', time().random_int(100000,999999).sha1(random_int(100000,999999).$_POST['psw']));
			$connectid = hash('sha256', time().random_int(100000,999999).sha1(random_int(100000,999999).$data['id']));
			$token = urlsafe_b64encode(hash('sha256', strval(random_int(100000,999999).$connectid), true));
			$created = time();
			$expire = $created+31557600;
			setcookie('session', $session, $expire, '/', NULL, false, true);
			setcookie('connectid', $connectid, $expire, '/', NULL, false, true);
			$req2 = $bdd2->prepare('INSERT INTO `sessions` (`account`, `session`, `connectid`, `expire`, `created`, `token`) VALUES (?,?,?,?,?,?)');
			$req2->execute(array($data['id'], password_hash($session,PASSWORD_DEFAULT), $connectid, $expire, $created, $token));
			header('Location: /redirlogin.php');
			exit();
		}
		else $log = 'Identifiants incorrects.';
	}
	else $log = 'Identifiants incorrects.';
}
if(isset($_GET['signed']) and isset($_GET['mail'])) {
	$req = $bdd2->prepare('SELECT `email` FROM `accounts` WHERE `id`=? AND `confirmed`=0 LIMIT 1');
	$req->execute(array($_GET['signed']));
	if($data = $req->fetch()) {
		if(sha1($data['email']) == $_GET['mail'])
			$log = 'Votre compte a bien été créé&#8239;! Vous devriez recevoir un e-mail de confirmation.';
	}
}
if(isset($_GET['confirmed']))
	$log = 'Votre adresse e-mail a bien été confirmée.';
elseif(isset($_GET['confirm_err']))
	$log = 'Le lien que vous avez suivi est invalide ou expiré. Veuillez recommencer.';
elseif(isset($_GET['logonly']))
	$log = 'Vous devez être identifié pour accéder à ce contenu.';
?>
<!doctype html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php include('include/banner.php');
include('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<?php if(!empty($log)) echo '<div id="divlog" role="complementary" aria-live="assertive"><p id="log"><b>'.$log.'</b></p></div>'; ?>
	<form action="?a=form#log" method="post">
		<input type="text" id="f1_username" name="username" placeholder="Nom d'utilisateur" maxlength="32" aria-label="Nom d'utilisateur" autofocus><br>
		<input type="password" id="f1_psw" name="psw" placeholder="Mot de passe" maxlength="64" aria-label="Mot de passe"><br>
		<input type="submit" id="f1_submit" value="S'identifier">
	</form>
	<a href="https://www.progaccess.net/mdp_demande.php">Vous avez oublié votre mot de passe&#8239;? Cliquez-ici&#8239;!( Redirection vers ProgAccess)</a><br>
	<a href="https://www.progaccess.net/signup.php">Pas encore de compte&#8239;? Inscrivez-vous et profitez des avantages&#8239;! (Redirection vers ProgAccess)</a>
	<p>En utilisant ce formulaire, vous devez accepter de recevoir les cookies de ProgAccess et <?php echo $site_name; ?> nécessaires à l'utilisation de l'espace membre.</p>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>