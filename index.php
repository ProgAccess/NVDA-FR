﻿<?php set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once('include/consts.php');
$stats_page = 'index';
$titre="NVDA, la meilleure réponse libre à l'accessibilité de l'informatique pour les aveugles et malvoyants."; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "include/header.php"; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<?php if(isset($_GET['act']) && $_GET['act'] == 'mailsend') echo '<p role="alert">Merci, votre message a bien été envoyé</p>'; ?>
<h2>Présentation de NVDA</h2>
<p>NonVisual desktop Access (NVDA) est une revue d'écran gratuite et opensource pour le système d'exploitation Microsoft Windows.<br>
En donnant des informations via une voix synthétique et/ou en Braille, il permet aux personnes déficientes visuelles d'accéder à un ordinateur sans coût additionnel par rapport à une personne voyante.<br>
NVDA est développé par <a href="https://www.nvaccess.org">NV Access</a>, avec des contributions de l'ensemble des utilisateurs.</p>
<p>Les principaux points forts de NVDA sont&nbsp;:</p>
<ul>
<li>La prise en charge d'applications grand-public telles que des navigateurs Web, clients e-mail, programmes de tchat sur Internet et suites bureautiques&nbsp;;</li>
<li>Un synthétiseur intégré (eSpeak NG) prenant en charge plus de 20 langues&nbsp;;</li>
<li>L'annonce, quand c'est possible, d'informations de mise en forme du texte telles que le nom et la taille de la police, le style et les fautes d'orthographe&nbsp;;</li>
<li>L'annonce automatique du texte sous la souris et, en option, l'indication sonore de la position de la souris&nbsp;;</li>
<li>La prise en charge de nombreux afficheurs braille&nbsp;;</li>
<li>La possibilité de s'exécuter entièrement depuis une clé USB ou tout autre média portable sans avoir à être installé&nbsp;;</li>
<li>Un installateur parlant facile à utiliser&nbsp;;</li>
<li>Une traduction dans environ 60 langues&nbsp;;</li>
<li>La prise en charge des environnements Windows modernes, incluant les versions 32 et 64 bits&nbsp;;</li>
<li>L'accès à l'écran de connexion à Windows et aux autres écrans sécurisés (tel que le contrôle de compte d'utilisateur)&nbsp;;</li>
<li>La prise en charge des interfaces d'accessibilités communes telles que Microsoft Active Accessibility, Java Access Bridge, IAccessible2 et UI Automation&nbsp;;</li>
<li>Le support de l'invite de commandes Windows et des applications en mode console.</li>
</ul>
<h2>Dernières actualités</h2>
<?php
$req = $bdd->query('SELECT * FROM softwares WHERE category=5 AND public=1 ORDER BY date DESC LIMIT 3');
echo '<ul>';
while($data = $req->fetch()) {
	echo '<li><a href="/article.php?id='.$data['id'].'">'.str_replace('{{site}}', $site_name, $data['name']).'</a>&nbsp;: publié le '.date('d/m/Y à H:i:s', $data['date']).'</li>';
}
echo '</ul>';
?>
<h2>Nos derniers tweets</h2>
<?php
require_once('include/lib/twitter/twitter.php');
$tweets = getTweets(10);
echo '<ul>';
foreach ($tweets as $tweet) {
    echo '<li>Le '.strftime('%d/%m/%Y à %H:%M', strtotime($tweet->created_at)).'&nbsp;: '.formatTweetText($tweet->text).'</li>';
}
echo '</ul>';
?>
</div>
<?php include "include/footer.php"; ?>
</div>
</body>
</html>