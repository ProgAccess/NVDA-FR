<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require_once 'include/lib/phpmailer/src/PHPMailer.php';
require_once 'include/lib/phpmailer/src/Exception.php';
require_once 'include/lib/phpmailer/src/SMTP.php';
$stats_page='article';
if(!isset($_GET['id'])) {header('Location: /');die();}
set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once 'include/consts.php';
require_once 'include/MDConverter.php';
require_once 'include/isbot.php';
	if(isset($logged) && $logged == 'true' AND $login['rank'] == 'a') {
		$req2 = $bdd2->prepare('SELECT `works` FROM `team` WHERE `account_id`=? LIMIT 1');
		$req2->execute(array($login['id']));
			if($data2 = $req2->fetch()) {
				$workn = $data2['works'];
			}
			$req2->closeCursor();
	}
$req = $bdd->prepare('SELECT * FROM softwares WHERE id=?');
$req->execute(array($_GET['id']));
$data = $req->fetch();
if(!$data){header('Location: /');die();}
if(!(isset($logged) and $logged and $settings['rank'] == 'a') and !$isbot) {
	$req = $bdd->prepare('UPDATE `softwares` SET `hits`=`hits`+1 WHERE `id`=? LIMIT 1');
	$req->execute(array($data['id']));
}
$titre = str_replace('{{site}}', $site_name, $data['name']);
$sft_info = $data;
$comlog = '';

if(isset($_GET['comment']) and isset($_POST['pseudo']) and isset($_POST['text'])) {
	if(strlen($_POST['pseudo']) <= 31 or strlen($_POST['text']) <= 1023) {
		$req = $bdd->prepare('INSERT INTO softwares_comments(sw_id,date,pseudo,text,ip) VALUES(?,?,?,?,?)');
		$req->execute(array($sft_info['id'], time(), htmlentities($_POST['pseudo']), $_POST['text'], sha1($_SERVER['REMOTE_ADDR'])));
		$comlog = 'Le commentaire a bien été envoyé.';
		header('Location: /a'.$sft_info['id']);
	}
	else $comlog = 'Le pseudo ne doit pas excéder 31 caractères.<br>Le texte ne doit pas excéder 1023 caractères.';
}
if(isset($_GET['cdel'])) {
	if(isset($logged) && $logged == 'true' AND $login['rank'] == 'a' AND $workn == '0' or $workn == '2') {
	$req = $bdd->prepare('DELETE FROM softwares_comments WHERE id=? LIMIT 1');
	$req->execute(array($_GET['cdel']));
	} else {
		$req2 = $bdd->prepare('DELETE FROM softwares_comments WHERE id=? AND date>? AND ip=? LIMIT 1');
	$req2->execute(array($_GET['cdel'], time()-86400, sha1($_SERVER['REMOTE_ADDR'])));
	}
}
if(isset($_GET['cedit2']) and isset($_POST['text'])) {
		if(isset($logged) && $logged == 'true' AND $login['rank'] == 'a' AND $workn == '0' or $workn == '2') {
	$req = $bdd->prepare('SELECT * FROM softwares_comments WHERE id=? LIMIT 1');
	$req->execute(array(htmlentities($_GET['cedit2'])));
		} else {
				$req = $bdd->prepare('SELECT * FROM softwares_comments WHERE id=? AND date>? AND ip=? LIMIT 1');
	$req->execute(array(htmlentities($_GET['cedit2']), time()-86400, sha1($_SERVER['REMOTE_ADDR'])));
		}
	if($data = $req->fetch()) {
		if(strlen($_POST['text']) <= 1023) {
			$req2 = $bdd->prepare('UPDATE softwares_comments SET text=? WHERE id=? LIMIT 1');
			$req2->execute(array(str_replace("\n",'<br/>',htmlentities($_POST['text'])), $data['id']));
			header('Location: /a'.$sft_info['id']);
		}
		else $comlog = 'Le texte ne doit pas excéder 1023 caractères.';
	} else $comlog = 'Erreur lors de la modification du commentaire.';
	$req->closeCursor();
}

$cat = array();
$req = $bdd->query('SELECT * FROM `softwares_categories`');
while($data = $req->fetch()) {
$cat[$data['id']] = $data['name'];
}
?>
<!doctype html>
<html lang="fr">
<?php include 'include/header.php'; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
			<?php
if(isset($logged) and $logged) {
	if(isset($login['rank']) && $login['rank'] == 'a') {
$req = $bdd2->prepare('SELECT `works` FROM `team` WHERE `account_id`=? LIMIT 1');
				$req->execute(array($login['id']));
				if($data = $req->fetch()) {
					$worksnum = $data['works'];
				}
				if($worksnum == '0' or $worksnum == '2') { ?>
<ul>
<li><a href="/admin/sw_mod.php?id=<?php echo $sft_info['id']; ?>"><?php echo str_replace('{{title}}', $site_name, 'Éditer '.$sft_info['name']); ?></a></li>
<li><a href="/admin/sw_mod.php?listfiles=<?php echo $sft_info['id']; ?>"><?php echo str_replace('{{title}}', $site_name, 'Afficher les fichiers de '.$sft_info['name']); ?></a></li>
</ul>
<?php
} } } ?>
<h2><?php echo $sft_info['description']; ?></h2>
<?php
function human_filesize($bytes, $decimals = 1) {
	$sz = ' kMGTP';
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}
echo '<div id="descart">';
echo convertToMD(str_replace('{{site}}', $site_name, $sft_info['text']));
echo '</div>';
$first = true;
$altc = true;
$req = $bdd->prepare('SELECT * FROM softwares_files WHERE sw_id=? ORDER BY date DESC');
$req->execute(array($sft_info['id']));
while($data = $req->fetch()) {
	if($first) {
		echo '<ul>';
		$fichiersexistants = true;
		$first = false;
	}
	echo '<li><a class="sw_file_link" href="/dl/';
	if(empty($data['label']))
		echo $data['id'];
	else
		echo $data['label'];
	echo '">Télécharger '.str_replace('{{site}}', $site_name, $data['title']).'</a> <span class="sw_file_size">('.str_replace('.', ',', human_filesize($data['filesize'])).'o)</span>, ajouté le '.date('d/m/Y à H:i:s',$data['date']);
	if(in_array($sft_info['category'], CAT_MOREINFO)) {
		$addon = json_decode($data['metadata'], true);
		echo '<ul><li>Auteur&nbsp;: '; if($sft_info['repo'] != '') { echo '<a href="'.$sft_info['repo'].'">'; } echo (!empty($addon['author'])?$addon['author']:'Aucune information disponible'); if($sft_info['repo'] != '') { echo '</a>'; } echo '</li><li>Version&nbsp;: '.(!empty($addon['version'])?$addon['version']:'Aucune information disponible').'</li><li>Compatibilité&nbsp;: '.((!empty($addon['NVDAMin']) && !empty($addon['NVDAMax']))?'de NVDA '.str_replace(".0", "", $addon['NVDAMin']).' à '.str_replace(".0", "", $addon['NVDAMax']):'Aucune information disponible').'</li><li>Type&nbsp;: '.$sft_info['website'].'</li></ul>';
	}
	echo '</li>';
	$altc = !$altc;
}
if(!$first)
	echo '</ul>';

$first = true;
$altc = true;
$req = $bdd->prepare('SELECT * FROM softwares_mirrors WHERE sw_id=? ORDER BY hits DESC');
$req->execute(array($sft_info['id']));
while($data = $req->fetch()) {
	if($first) {
		echo '<table id="sw_mirrors"><caption><strong>Fichiers en miroir pour '.$titre.'&nbsp;:</strong></caption><thead><tr><th>Titre</th><th>Miroirs</th><th>Modifié le</th><th>Téléchargements</th></tr></thead><tbody>';
		$first = false;
	}
	echo '<tr class="sw_file';
	if($altc) echo ' altc';
	echo '"><td class="sw_file_title"><a class="sw_file_link" href="/r.php?m&';
	if(empty($data['label']))
		echo 'id='.$data['id'];
	else
		echo 'p='.$data['label'];
	echo '">'.str_replace('{{site}}', $site_name, $data['title']).'</a></td><td class="sw_file_ltd">';
	$i = 0;
	$links = json_decode($data['links'], true);
	foreach($links as $link) {
		if($i != 0)
			echo ' | ';
		echo '<a class="sw_file_link" href="/r.php?m='.$i.'&';
		if(empty($data['label']))
			echo 'id='.$data['id'];
		else
			echo 'p='.$data['label'];
		echo '" download="'.$link[0].'">'.$link[0].'</a>';
		$i ++;
	}
	echo '</td><td class="sw_file_date">'.date('d/m/Y à H:i:s',$data['date']).'</td><td class="sw_file_hits">'.$data['hits'].'</td></tr>';
	$altc = !$altc;
}
if(!$first)
	echo '</tbody></table>';
?>
<table><caption>Informations de l'article&nbsp;:</caption>
<tbody>
<?php if(isset($fichiersexistants) && $fichiersexistants == true) { echo '<tr><td>Nombre total de téléchargements&nbsp;:</td>
<td>'.$sft_info['downloads'].'</td></tr>'; } ?>
<tr><td>Nombre total de visites&nbsp;:</td>
<td><?php echo $sft_info['hits']; ?></td></tr>
<tr><td>Catégorie&nbsp;:</td>
<td><?php echo '<a href="/c'.$sft_info['category'].'">'.$cat[$sft_info['category']].'</a>'; ?></td></tr>
<tr><td>Dernière modification&nbsp;:</td>
<td>Le <?php echo date('d/m/Y à H:i:s',$sft_info['date']); ?>, par <?php print $sft_info['author']; ?></td></tr>
</tbody></table>
			<h2>Commenter <?php print $titre; ?></h2>
			<div id="comments">
				<?php
$req = $bdd->prepare('SELECT * FROM softwares_comments WHERE sw_id=? ORDER BY date DESC LIMIT 20');
$req->execute(array($sft_info['id']));
while($data = $req->fetch()) {
	echo '<div class="comment"><div class="comment_h"><h3>';
	echo htmlentities($data['pseudo']);
	echo ' ('.date('d/m/Y, H:i:s', $data['date']).')</h3>';
	echo '</span></div>';
	echo '<p class="comment_p">'.str_replace("\n",'<br>',html_entity_decode($data['text'])).'</p></div>';
		if(($data['ip'] == sha1($_SERVER['REMOTE_ADDR']) and $data['date'] > time()-86400) OR (isset($logged) && $logged == 'true' AND $login['rank'] == 'a' AND $workn == '0' or $workn == '2')) {
		echo '<a href="?id='.$sft_info['id'].'&cedit='.$data['id'].'#cedit"><img alt="Modifier le commentaire" src="https://zettascript.org/images/mod16.png"></a><a href="?id='.$sft_info['id'].'&cdel='.$data['id'].'"><img alt="Supprimer le commentaire" src="https://zettascript.org/images/trash16.png"></a>';
	}
}
$req->closeCursor();

if(isset($_GET['cedit'])) {
	if(isset($logged) && $logged == 'true' AND $login['rank'] == 'a' AND $workn == '0' or $workn == '2') {
	$req = $bdd->prepare('SELECT id, text FROM softwares_comments WHERE id=?');
	$req->execute(array($_GET['cedit']));
	} else {
		$req = $bdd->prepare('SELECT id, text FROM softwares_comments WHERE id=? AND date>? AND ip=?');
	$req->execute(array($_GET['cedit'], time()-86400, sha1($_SERVER['REMOTE_ADDR'])));
	}
	if($data = $req->fetch()) {
?>
				<form action="?id=<?php echo $sft_info['id'].'&cedit2='.$data['id'] ?>" method="post" id="cedit">
					<fieldset><legend>Modifier le commentaire</legend>
						<label for="fc_text">Texte&nbsp;:</label><br>
						<textarea id="fc_text" class="ta" name="text" maxlength="1023"><?php echo html_entity_decode($data['text']); ?></textarea><br>
						<input type="submit" value="Envoyer">
					</fieldset>
				</form>
<?php }$req->closeCursor();}
if(isset($logged) && $logged == 'true') { ?>
	<form action="?id=<?php echo $sft_info['id'] ?>&comment" method="post" id="comment_write">
					<?php if($comlog!='') echo '<strong>'.$comlog.'</strong>'; ?>
					<fieldset><legend>Envoyer un commentaire</legend>
						<p>Écrivez ici votre commentaire sur cet article. Vous aurez la possibilité de modifier ou de supprimer votre commentaire pendant 24h après la publication.</p>
						<label for="fc_pseudo">Pseudo&nbsp;:</label>
						<input type="text" id="fc_pseudo" name="pseudo" maxlength="31"<?php echo ' value="'.$login['username'].'"'; ?> readonly><br>
						<label for="fc_text">Texte&nbsp;:</label><br>
						<textarea id="fc_text" class="ta" name="text" maxlength="1023"><?php if(isset($_POST['text']) and strlen($_POST['text']) <= 1023) echo htmlentities($_POST['text']); ?></textarea><br>
						<input type="submit" value="Envoyer">
					</fieldset>
				</form>
<?php } else { ?>
<p>Le dépôt de commentaires sur <?php print $site_name; ?> est réservé aux membres. Merci de <a href="/login.php">vous connecter</a>, ou de <a href="https://www.progaccess.net/signup.php">vous inscrire</a>.<br>
Rappel&nbsp;: votre compte membre NVDA-FR est identique à votre compte membre <a href="https://www.progaccess.net">ProgAccess</a>.</p>
<?php } ?>
			</div>
</div>
<?php include 'include/footer.php'; ?>
</div>
</body>
</html>