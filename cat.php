<?php
if(!isset($_GET['id'])) {header('Location: /');die();}
set_include_path($_SERVER['DOCUMENT_ROOT']);
require_once('include/log.php');
require_once 'include/consts.php';
$req = $bdd->prepare('SELECT * FROM softwares_categories WHERE id=?');
$req->execute(array($_GET['id']));
$data = $req->fetch();
if(!$data){header('Location: /');die();}
$cat_id = $data['id'];
$titre = str_replace('{{site}}', $site_name, $data['name']);
$cat_text = $data['text'];

$stats_page='cat'; ?>
<!doctype html>
<html lang="fr">
<?php require_once 'include/header.php'; ?>
<body>
<div id="container">
<?php require_once('include/banner.php');
require_once('include/menu.php'); ?>
<div id="body" role="main">
<h1 id="contenu"><?php print $titre; ?></h1>
<?php
echo str_replace('{{site}}', $site_name, $cat_text);
$req = $bdd->prepare('SELECT * FROM softwares WHERE category=? AND public=1 ORDER BY date DESC');
$req->execute(array($cat_id));

while($data = $req->fetch()) {
		echo '<div class="software"><h2 class="software_title"><a href="a'.$data['id'].'">'.str_replace('{{site}}', $site_name, $data['name']).'</a>';
echo '</h2>';
	echo '<p>'.str_replace('{{site}}', $site_name, $data['description']).'<br><span class="software_hits">'.$data['hits'].' visites</span><span class="software_date"> (modifié le '.date('d/m/Y à H:i:s', $data['date']).')</span></p></div>';
}
?>
</div>
<?php include 'include/footer.php'; ?>
</div>
</body>
</html>
